﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Services.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.WindowsForms.Helper
{
    public class ServiceAccess
    {
        private DatabaseFactory dbfactory;
        private IUnitOfWork unitOfWork;
        public ServiceAccess()
        {
            dbfactory = new DatabaseFactory();
            unitOfWork = new UnitOfWork(dbfactory);
        }
        public IUnitOfWork UnitOfWork { get { return unitOfWork; } }
        public ICustomerService CustomerService { get { return new CustomerService(new CustomerRepository(dbfactory), unitOfWork); } }
        public IEmployeeService EmployeeService { get { return new EmployeeService(new EmployeeRepository(dbfactory), unitOfWork); } }
        public IProductDetailsService ProductDetailsService { get { return new ProductDetailsService(new ProductDetailsRepository(dbfactory), unitOfWork); } }
        public IProductSalesServise ProductSalesServise { get { return new ProductSalesServise(new ProductSalesRepository(dbfactory), unitOfWork); } }
        public IRawMaterialBuyService RawMaterialBuyService { get { return new RawMaterialBuyService(new RawMaterialBuyRepository(dbfactory), unitOfWork); } }
        public IRawMaterialHandoverService RawMaterialHandoverService { get { return new RawMaterialHandoverService(new RawMaterialHandoverRepository(dbfactory), unitOfWork); } }
        public IRawMaterialService RawMaterialService { get { return new RawMaterialService(new RawMaterialRepository(dbfactory), unitOfWork); } }
        public IRequestedProductService RequestedProductService { get { return new RequestedProductService(new RequestedProductRepository(dbfactory), unitOfWork); } }
        public IRequestRawMaterialService RequestRawMaterialService { get { return new RequestRawMaterialService(new RequestRawMaterialRepository(dbfactory), unitOfWork); } }
        public ISalaryPaymentServise SalaryPaymentServise { get { return new SalaryPaymentServise(new SalaryPaymentRepository(dbfactory), unitOfWork); } }
        public ISalaryService SalaryService { get { return new SalaryService(new SalaryRepository(dbfactory), unitOfWork); } }
        public IStoredProductServise StoredProductServise { get { return new StoredProductServise(new StoredProductRepository(dbfactory), unitOfWork); } }
        public IStoredRawMaterialServise StoredRawMaterialServise { get { return new StoredRawMaterialServise(new StoredRawMaterialRepository(dbfactory), unitOfWork); } }
        public IProductionService ProductionService { get { return new ProductionService(new ProductionRepository(dbfactory), unitOfWork); } }
        public IUnitService UnitService { get { return new UnitService(new UnitRepository(dbfactory), unitOfWork); } }
        public IProductRawmaterialMappingService ProductRawmaterialMappingService { get { return new ProductRawmaterialMappingService(new ProductRawmaterialMappingRepository(dbfactory), unitOfWork); } }
        public IProductOrderedService ProductOrderedService { get { return new ProductOrderedService(new ProductOrderedRepository(dbfactory), unitOfWork); } }
    }
}