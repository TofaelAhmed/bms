﻿using BMS.Globals.Utilities;
using BMS.WindowsForms.UI.UserControls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMS.WindowsForms.UI
{
    public partial class MainUi : Form
    {
        public MainUi()
        {
            InitializeComponent();
        }
        private void closeControlLabel_Click(object sender, EventArgs e)
        {
            controlPanel.Controls.Clear();
            closeControlLabel.Visible = false;
        }
        private void DetailedInformationOnRawMaterialsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RawMaterialControl control = new RawMaterialControl();
            ShowControl(control, @"কাঁচামালের বিস্তারিত তথ্য"); 
        }
        private void ByeRawToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RawMaterialBuyControl control = new RawMaterialBuyControl();
            ShowControl(control, @"কাঁচামাল ক্রয়ের তথ্য");
        }
        private void RequestedRawMaterialsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RequestedRawMaterialControl control = new RequestedRawMaterialControl();
            ShowControl(control, @"অনুরোধকৃত কাঁচামালের তথ্য");
        }
        private void DeliveryRawMaterialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RawMaterialHandoverControl control = new RawMaterialHandoverControl();
            ShowControl(control, @"কাঁচামাল হস্তান্তেরের তথ্য");
        }
        private void RawMaterialStockeinfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StoredRawMaterialControl control = new StoredRawMaterialControl();
            ShowControl(control, @"মজুদকৃত কাঁচামালের তথ্য");
        }
        private void ProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductDetailsControl control = new ProductDetailsControl();
            ShowControl(control, @"পণ্যের বিস্তারিত তথ্য");
        }
        private void ProductionInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void RequestForProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RequestForProduct control = new RequestForProduct();
            ShowControl(control, @"অনুরোধকৃত পণ্যের তথ্য");
        }
        private void StokedProductInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StoredProductControl control = new StoredProductControl();
            ShowControl(control, @"মজুদকৃত পণ্যের তথ্য");
        }
        private void RequestForRawMetarialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RequestRawMaterialControl control = new RequestRawMaterialControl();
            ShowControl(control, @"অনুরোধকৃত কাঁচামালের তথ্য");
        }
        private void ByerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomarControl control = new CustomarControl();
            ShowControl(control, @"ক্রেতার তথ্য");
        }
        private void SalesProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductSalesControl control = new ProductSalesControl();
            ShowControl(control, @"পণ্য বিক্রয়ের তথ্য");


        }
        private void DesignationToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void CustomerDuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomarDewAmountInformation control = new CustomarDewAmountInformation();
            ShowControl(control, @"ক্রেতার বাকীর পরিমাণ");
        }
        private void DuesClearHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PricePaymentControl control = new PricePaymentControl();
            ShowControl(control, @"মুল্য পরিশোধের তথ্য");
        }
        private void EmployeeInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeControl control = new EmployeeControl();
            ShowControl(control, @"কর্মচারির তথ্য");
        }
        private void PaidSalaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SalaryPaymentControl control = new SalaryPaymentControl();
            ShowControl(control, @"বেতন পরিশোধের তথ্য");
        }
        private void SalaryInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SalaryControl control = new SalaryControl();
            ShowControl(control, @"বেতনরে তথ্য");
        }
        private void SalaryDuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PendingSalaryControl control = new PendingSalaryControl();
            ShowControl(control, @"বকেয়া বেতন");
        }
        public void ShowControl(Control control, string title)
        {
            closeControlLabel.Visible = true;
            //controlPanel.Controls.Clear();
            //controlPanel.Controls.Add(control);
            //control.Dock = DockStyle.Fill;
            //control.Focus();
            //control.BringToFront();
            if (!controlPanel.Controls.Contains(control))
            {
                controlPanel.Controls.Add(control);
                control.Dock = DockStyle.Fill;
                control.BringToFront();
                control.Focus();
            }
            else
            {
                control.BringToFront();
            }
            Text = title;

        }

        private void ProductOrderControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductOrderControl control = new ProductOrderControl();
            ShowControl(control, @"পণ্য জন্য অনুরোধ");
        }
    }
}