﻿using BMS.Entities.BMS;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class ProductEntry : Form
    {
        ServiceAccess service = new ServiceAccess();
        public ProductEntry()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(comboboxdataload);
            BL.Start();
        }
        public void comboboxdataload()
        {
            var item = service.UnitService.GetUnits();
            var modified = item.Select(x => new {
                UnitName = x.UnitName,
                UnitId = x.UnitId
            }).ToList();

            unitNameComboBox.DataSource = new BindingSource(modified, null);
            unitNameComboBox.DisplayMember = "UnitName";
            unitNameComboBox.ValueMember = "UnitId";
            unitNameComboBox.SelectedIndex = -1;
        }
        //private void saveButton_Click(object sender, EventArgs e)
        //{
        //    ProductDetails obj = new ProductDetails();
        //    obj.ProductName = productNameTextBox.Text.Trim();
        //    obj.UnitId = Convert.ToInt32(unitNameComboBox.SelectedValue.ToString());
        //    obj.ProductRemark = remarrkTextBox.Text.Trim();
        //    service.ProductDetailsService.Save(obj);
        //    service.UnitOfWork.Commit();
        //    //if ()
        //    //{
        //    //    MessageBox.Show("Product Save Sucessfully......!");
        //    //}
        //    //else
        //    //{

        //    //}
            
        //}
        private void closeButton_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                ProductDetails obj = new ProductDetails();
                obj.ProductName = productNameTextBox.Text.Trim();
                obj.UnitId = Convert.ToInt32(unitNameComboBox.SelectedValue.ToString());
                obj.ProductRemark = remarrkTextBox.Text.Trim();

                service.ProductDetailsService.Save(obj);
                service.UnitOfWork.Commit();
                msgLabel.ForeColor = Color.Green;
                MessageBox.Show("সঠিক যুক্ত হয়েছে............!");
                this.Close();
            }
            catch (Exception ex)
            {
                msgLabel.ForeColor = Color.Red;
                MessageBox.Show("যুক্ত হয়নি.........!");
            }
        }
    }
}