﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class SalaryControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public SalaryControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(display);
            BL.Start();
            GridviewDesign.Instance(salaryInformationdataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            display();
            comboboxdataload();
        }
        private void Save_Click(object sender, EventArgs e)
        {
            Salary obj = new Salary();
            obj.DesignationName = employeeDesignationComboBox.SelectedItem.ToString();
            obj.SalaryAmount = Convert.ToInt64(salaryAmountTextBox.Text.Trim());
            service.SalaryService.Save(obj);
            service.UnitOfWork.Commit();
            display();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            employeeDesignationComboBox.ResetText();
            salaryAmountTextBox.Clear();
        }
        void display()
        {
            salaryInformationdataGridView.AutoGenerateColumns = false;
            salaryInformationdataGridView.DataSource = service.SalaryService.GetSalaryServices();
        }
        void comboboxdataload()
        {
            employeeDesignationComboBox.DataSource = new BindingSource(service.EmployeeService.GetEmployees(), null);
            employeeDesignationComboBox.DisplayMember = "EmployeeDesignation";
            employeeDesignationComboBox.ValueMember = "EmployeeId";
            employeeDesignationComboBox.SelectedIndex = -1;
        }
    }
}
