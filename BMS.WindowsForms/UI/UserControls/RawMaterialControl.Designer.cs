﻿namespace BMS.WindowsForms.UI.UserControls
{
    partial class RawMaterialControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rawMaterialControlpanel = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.inputRawlabel = new System.Windows.Forms.Label();
            this.unitNameComboBox = new System.Windows.Forms.ComboBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.rawMaterialRemarkTextBox = new System.Windows.Forms.TextBox();
            this.rawMaterialNameTextBox = new System.Windows.Forms.TextBox();
            this.Save = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblPageNumber = new System.Windows.Forms.Label();
            this.rowMaterialInfoDataGridView = new System.Windows.Forms.DataGridView();
            this.RawMaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RawMaterialUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RawMaterialRemark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rawMaterialControlpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rowMaterialInfoDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // rawMaterialControlpanel
            // 
            this.rawMaterialControlpanel.AutoScroll = true;
            this.rawMaterialControlpanel.AutoSize = true;
            this.rawMaterialControlpanel.BackColor = System.Drawing.Color.PowderBlue;
            this.rawMaterialControlpanel.Controls.Add(this.splitContainer1);
            this.rawMaterialControlpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rawMaterialControlpanel.Location = new System.Drawing.Point(0, 0);
            this.rawMaterialControlpanel.Name = "rawMaterialControlpanel";
            this.rawMaterialControlpanel.Size = new System.Drawing.Size(1009, 685);
            this.rawMaterialControlpanel.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.inputRawlabel);
            this.splitContainer1.Panel1.Controls.Add(this.unitNameComboBox);
            this.splitContainer1.Panel1.Controls.Add(this.deleteButton);
            this.splitContainer1.Panel1.Controls.Add(this.rawMaterialRemarkTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.rawMaterialNameTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.Save);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.splitContainer1.Panel2.Controls.Add(this.lblPageNumber);
            this.splitContainer1.Panel2.Controls.Add(this.rowMaterialInfoDataGridView);
            this.splitContainer1.Size = new System.Drawing.Size(1009, 685);
            this.splitContainer1.SplitterDistance = 217;
            this.splitContainer1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(67, 51);
            this.label5.MaximumSize = new System.Drawing.Size(15, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 15);
            this.label5.TabIndex = 20;
            this.label5.Text = "*";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(62, 12);
            this.label4.MaximumSize = new System.Drawing.Size(15, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "*";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // inputRawlabel
            // 
            this.inputRawlabel.AutoSize = true;
            this.inputRawlabel.Location = new System.Drawing.Point(15, 229);
            this.inputRawlabel.Name = "inputRawlabel";
            this.inputRawlabel.Size = new System.Drawing.Size(0, 13);
            this.inputRawlabel.TabIndex = 19;
            this.inputRawlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // unitNameComboBox
            // 
            this.unitNameComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unitNameComboBox.FormattingEnabled = true;
            this.unitNameComboBox.Location = new System.Drawing.Point(8, 68);
            this.unitNameComboBox.Name = "unitNameComboBox";
            this.unitNameComboBox.Size = new System.Drawing.Size(197, 21);
            this.unitNameComboBox.TabIndex = 2;
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(27)))), ((int)(((byte)(76)))));
            this.deleteButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteButton.FlatAppearance.BorderSize = 0;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deleteButton.Location = new System.Drawing.Point(94, 187);
            this.deleteButton.Margin = new System.Windows.Forms.Padding(0);
            this.deleteButton.MaximumSize = new System.Drawing.Size(68, 22);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Padding = new System.Windows.Forms.Padding(2);
            this.deleteButton.Size = new System.Drawing.Size(43, 19);
            this.deleteButton.TabIndex = 5;
            this.deleteButton.Text = "মুছুন";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // rawMaterialRemarkTextBox
            // 
            this.rawMaterialRemarkTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rawMaterialRemarkTextBox.BackColor = System.Drawing.Color.GhostWhite;
            this.rawMaterialRemarkTextBox.Location = new System.Drawing.Point(6, 111);
            this.rawMaterialRemarkTextBox.Multiline = true;
            this.rawMaterialRemarkTextBox.Name = "rawMaterialRemarkTextBox";
            this.rawMaterialRemarkTextBox.Size = new System.Drawing.Size(198, 62);
            this.rawMaterialRemarkTextBox.TabIndex = 3;
            // 
            // rawMaterialNameTextBox
            // 
            this.rawMaterialNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rawMaterialNameTextBox.BackColor = System.Drawing.Color.GhostWhite;
            this.rawMaterialNameTextBox.Location = new System.Drawing.Point(8, 29);
            this.rawMaterialNameTextBox.Name = "rawMaterialNameTextBox";
            this.rawMaterialNameTextBox.Size = new System.Drawing.Size(197, 20);
            this.rawMaterialNameTextBox.TabIndex = 1;
            // 
            // Save
            // 
            this.Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Save.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(27)))), ((int)(((byte)(76)))));
            this.Save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Save.FlatAppearance.BorderSize = 0;
            this.Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Save.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Save.Location = new System.Drawing.Point(146, 187);
            this.Save.Margin = new System.Windows.Forms.Padding(0);
            this.Save.MaximumSize = new System.Drawing.Size(68, 22);
            this.Save.Name = "Save";
            this.Save.Padding = new System.Windows.Forms.Padding(2);
            this.Save.Size = new System.Drawing.Size(55, 19);
            this.Save.TabIndex = 4;
            this.Save.Text = "যুক্ত করুণ";
            this.Save.UseVisualStyleBackColor = false;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "মন্তব্য";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(5, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "পরিমাপের একক";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "কাঁচামালের নাম";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(215, 0);
            this.panel2.TabIndex = 0;
            // 
            // lblPageNumber
            // 
            this.lblPageNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPageNumber.AutoSize = true;
            this.lblPageNumber.Location = new System.Drawing.Point(319, 420);
            this.lblPageNumber.Name = "lblPageNumber";
            this.lblPageNumber.Size = new System.Drawing.Size(0, 13);
            this.lblPageNumber.TabIndex = 2;
            // 
            // rowMaterialInfoDataGridView
            // 
            this.rowMaterialInfoDataGridView.AllowUserToAddRows = false;
            this.rowMaterialInfoDataGridView.AllowUserToDeleteRows = false;
            this.rowMaterialInfoDataGridView.AllowUserToResizeColumns = false;
            this.rowMaterialInfoDataGridView.AllowUserToResizeRows = false;
            this.rowMaterialInfoDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rowMaterialInfoDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.rowMaterialInfoDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.rowMaterialInfoDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.rowMaterialInfoDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rowMaterialInfoDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.rowMaterialInfoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rowMaterialInfoDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RawMaterialName,
            this.RawMaterialUnit,
            this.RawMaterialRemark});
            this.rowMaterialInfoDataGridView.GridColor = System.Drawing.SystemColors.ControlText;
            this.rowMaterialInfoDataGridView.Location = new System.Drawing.Point(0, 0);
            this.rowMaterialInfoDataGridView.MultiSelect = false;
            this.rowMaterialInfoDataGridView.Name = "rowMaterialInfoDataGridView";
            this.rowMaterialInfoDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rowMaterialInfoDataGridView.Size = new System.Drawing.Size(786, 568);
            this.rowMaterialInfoDataGridView.TabIndex = 0;
            // 
            // RawMaterialName
            // 
            this.RawMaterialName.DataPropertyName = "RawMaterialName";
            this.RawMaterialName.HeaderText = "কাঁচামালের নাম";
            this.RawMaterialName.Name = "RawMaterialName";
            // 
            // RawMaterialUnit
            // 
            this.RawMaterialUnit.DataPropertyName = "UnitName";
            this.RawMaterialUnit.HeaderText = "পরিমাপের একক";
            this.RawMaterialUnit.Name = "RawMaterialUnit";
            // 
            // RawMaterialRemark
            // 
            this.RawMaterialRemark.DataPropertyName = "RawMaterialRemark";
            this.RawMaterialRemark.HeaderText = "মন্তব্য";
            this.RawMaterialRemark.Name = "RawMaterialRemark";
            // 
            // RawMaterialControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rawMaterialControlpanel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "RawMaterialControl";
            this.Size = new System.Drawing.Size(1009, 685);
            this.rawMaterialControlpanel.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rowMaterialInfoDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel rawMaterialControlpanel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox rawMaterialNameTextBox;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView rowMaterialInfoDataGridView;
        private System.Windows.Forms.TextBox rawMaterialRemarkTextBox;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.ComboBox unitNameComboBox;
        private System.Windows.Forms.Label lblPageNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn RawMaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RawMaterialUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn RawMaterialRemark;
        private System.Windows.Forms.Label inputRawlabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}
