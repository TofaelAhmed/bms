﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class RawMaterialHandoverControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public RawMaterialHandoverControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(display);
            BL.Start();
            GridviewDesign.Instance(rawMaterialHandoverdataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            comboboxdataload();
            display();
        }
        private void Save_Click(object sender, EventArgs e)
        {
            DateTime dtime;
            dtime = rawMaterialHandoverDateTimePicker.Value.Date;
            RawMaterialHandover obj = new RawMaterialHandover();
            obj.RawMaterialHandoverDate = dtime;
            obj.RawMaterialHandoverName = rawMatarialNameComboBox.Text.Trim();
            obj.RawMaterialHandoverUnit = rawMaterialHandoverUnitTextBox.Text.Trim();
            obj.RawMaterialHandoverRemark = rawMaterialHandoverRemarkTextBox.Text.Trim();
            service.RawMaterialHandoverService.Save(obj);
            service.UnitOfWork.Commit();
            display();
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
            rawMaterialHandoverDateTimePicker.ResetText();
            rawMatarialNameComboBox.ResetText();
            rawMaterialHandoverUnitTextBox.Clear();
            rawMaterialHandoverRemarkTextBox.Clear();
        }
        void display()
        {
            rawMaterialHandoverdataGridView.AutoGenerateColumns = false;
            rawMaterialHandoverdataGridView.DataSource = service.RawMaterialHandoverService.GetRawMaterialHandovers();
        }
        public void comboboxdataload()
        {
            var item = service.RawMaterialService.GetRawMaterials();
            var modified = item.Select(x => new {RawMaterialName = x.RawMaterialName, RawMaterialId = x.RawMaterialId}).ToList();
            rawMatarialNameComboBox.DataSource = new BindingSource(modified, null);
            rawMatarialNameComboBox.DisplayMember = "RawMaterialName";
            rawMatarialNameComboBox.ValueMember = "RawMaterialName";
            rawMatarialNameComboBox.SelectedIndex = -1;
        }
    }
}