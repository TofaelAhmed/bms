﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class RawMaterialControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public RawMaterialControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(display);
            BL.Start();
            GridviewDesign.Instance(rowMaterialInfoDataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            comboboxdataload();
        }
        private void Save_Click(object sender, EventArgs e)
        {
            if(IsValid())
            {
                RawMaterial obj = new RawMaterial();
                obj.RawMaterialName = rawMaterialNameTextBox.Text.Trim();
                obj.UnitId = Convert.ToInt32(unitNameComboBox.SelectedValue.ToString());
                obj.RawMaterialRemark = rawMaterialRemarkTextBox.Text.Trim();
                service.RawMaterialService.Save(obj);
                service.UnitOfWork.Commit();
                display();
            }
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
            rawMaterialNameTextBox.Clear();
            unitNameComboBox.ResetText();
            rawMaterialRemarkTextBox.Clear();
        }
        public void display()
        {
            
            var data = service.RawMaterialService.GetRawMaterials();
            var modified = data.Select(x => new
            {
                RawMaterialName = x.RawMaterialName,
                RawMaterialRemark = x.RawMaterialRemark,
                UnitName = x.Units.UnitName
            }).ToList();
            rowMaterialInfoDataGridView.DataSource = modified;
            rowMaterialInfoDataGridView.AutoGenerateColumns = false;
        }
        public void comboboxdataload()
        {
            var item = service.UnitService.GetUnits();
            var modified = item.Select(x => new { UnitName = x.UnitName, UnitId = x.UnitId }).ToList();

            unitNameComboBox.DataSource = new BindingSource(modified, null);
            unitNameComboBox.DisplayMember = "UnitName";
            unitNameComboBox.ValueMember = "UnitId";
            unitNameComboBox.SelectedIndex = -1;
        }
        private bool IsValid()
        {
            inputRawlabel.Text = "";
            inputRawlabel.ForeColor = Color.Red;
            
            if (!rawMaterialNameTextBox.Text.IsEmpty())
            {
                inputRawlabel.Text = " কাঁচামালের নাম লিখুন ";
                return false;
            }
            if (!unitNameComboBox.Text.IsEmpty() || unitNameComboBox.SelectedIndex < 0)
            {
                inputRawlabel.Text = " পরিমাপের একক নির্বাচন করুন ";
                return false;
            }
            inputRawlabel.ForeColor = Color.Black;
            return true;
        }
    }
}
