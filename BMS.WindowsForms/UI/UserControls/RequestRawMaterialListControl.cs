﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class RequestRawMaterialListControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();

        public RequestRawMaterialListControl()
        {
            InitializeComponent();
            productInfodataGridView.AutoGenerateColumns = false;
            BackgroundLoader BL = new BackgroundLoader(display);
            BL.Start();
            SplitContainerDesign.Instance(splitContainer1);
            comboboxdataload();
        }
        private bool IsValid()
        {
            inputRawlabel.Text = "";
            inputRawlabel.ForeColor = Color.Red;
            if (!productionComboBox.Text.IsEmpty() || productionComboBox.SelectedIndex < 0)
            {
                inputRawlabel.Text = " কাঁচামালের নাম লিখুন  ";
                return false;
            }
            if (!qtyTextBox.Text.IsEmpty())
            {
                inputRawlabel.Text = " পরিমান নির্বাচন করুন ";
                return false;
            }
            if (!qtyTextBox.Text.IsEmpty())
            {
                inputRawlabel.Text = " মন্তব লিখুন ";
                return false;
            }
            inputRawlabel.ForeColor = Color.Black;
            return true;
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                RequestRawMaterial obj = new RequestRawMaterial();
                DateTime dtime;
                dtime = prodactionDateTimePicker.Value.Date;
                obj.RequestRawMaterialDate = dtime;
                obj.RequestRawMaterialId = Convert.ToInt32(productionComboBox.SelectedValue.ToString());
                obj.RequestRawMaterialQty = Convert.ToDecimal(qtyTextBox.Text.Trim());
                obj.RequestRawMaterialRemark = productionRemarkTextBox.Text.Trim();
                service.RequestRawMaterialService.Save(obj);
                service.UnitOfWork.Commit();
                //display();
            }
        }
        public void display()
        {
            var data = service.RequestRawMaterialService.GetRequestRawMaterials();
            var modified = data.Select(x => new {
                RequestRawMaterialDate = x.RequestRawMaterialDate,
                RawMaterialName = x.RawMaterials.RawMaterialName,
                RequestRawMaterialQty = x.RequestRawMaterialQty,
                RawMaterialRemark = x.RequestRawMaterialRemark
            }).ToList();
            productInfodataGridView.DataSource = modified;
            productInfodataGridView.AutoGenerateColumns = false;
            //productInfodataGridView.DataSource = service.RequestRawMaterialService.GetRequestRawMaterials();
        }

        public void comboboxdataload()
        {
            var item = service.RawMaterialService.GetRawMaterials();
            var modified = item.Select(x => new { RawMaterialName = x.RawMaterialName, RawMaterialId = x.RawMaterialId });
            productionComboBox.DataSource = new BindingSource(modified, null);
            productionComboBox.DisplayMember = "RawMaterialName";
            productionComboBox.ValueMember = "RawMaterialId";
            productionComboBox.SelectedIndex = -1;
        }
    }
}
