﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.WindowsForms.Helper;
using BMS.Globals.Utilities;
using BMS.Entities.BMS;
using BMS.Entities.ViewModel.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class ProductOrderControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        List<ProductOrderVM> items = new List<ProductOrderVM>();
        
        public ProductOrderControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(productComboboxDataLoad);
            BL.Start();
            GridviewDesign.Instance(orderListDataGridView);
            productOrderListTextBox.Text = "00001212";
            orderListDataGridView.AutoGenerateColumns = false;
        }

        public void productComboboxDataLoad()
        {
            var item = service.ProductDetailsService.GetProductDetails();
            var modified = item.Select(x => new { ProductName = x.ProductName, ProductId = x.ProductId }).ToList();
            productNameComboBox.DataSource = new BindingSource(modified, null);
            productNameComboBox.DisplayMember = "ProductName";
            productNameComboBox.ValueMember = "ProductId";
            productNameComboBox.SelectedIndex = -1;
        }
        private void productNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                unitNameTextBox.Text = "";
                if (productNameComboBox.SelectedIndex < 0) return;
                var id = productNameComboBox.SelectedValue.ToString();
                decimal num;
                var isValidNum = decimal.TryParse(id, out num);
                if (isValidNum)
                {
                    var productid = Convert.ToInt32(productNameComboBox.SelectedValue);
                    var item = service.ProductDetailsService.GetProductDetailById(productid);
                    var unit = service.UnitService.GetUnitsById(item.UnitId);
                    unitNameTextBox.Text = unit.UnitName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool IsValid()
        {
            msgLabelValidation.Text = "";
            msgLabelValidation.ForeColor = Color.Red;
            if (!productNameComboBox.Text.IsEmpty() || productNameComboBox.SelectedIndex < 0)
            {
                msgLabelValidation.Text = "পরিমাপের একক নির্ণয় করুণ";
                return false;
            }
            if (!qtyTextBox.Text.IsEmpty())
            {
                msgLabelValidation.Text = "কাঁচামালের নাম লিখুন";
                return false;
            }
            msgLabelValidation.ForeColor = Color.Black;
            return true;
        }

        private void qtyTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 2404)
            {
                ch = '.';
                e.KeyChar = '.';
            }
            if (!ch.IsValidDigit())
            {
                msgLabelValidation.ForeColor = Color.Red;
                e.Handled = true;
                msgLabelValidation.Text = @" সঠিক সংখ্যা লিখুন";
            }
            else
            {
                if (!ch.ToString().IsValidNumber() && ch != 8 && ch != 46)
                {
                    e.KeyChar = (char)('0' + e.KeyChar - '\u09E6');
                }
                msgLabelValidation.Text = "";
            }
        }
        private void submitButton_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                DateTime rtime;
                rtime = requestDateTimePicker.Value.Date;
                DateTime dtime;
                dtime = delivetyDateTimePicker.Value.Date;
                ProductOrderVM addobj = new ProductOrderVM();
                addobj.OrderNumber = productOrderListTextBox.Text.ToString();
                addobj.RequestDate = rtime;
                addobj.DeliveryDate = dtime;
                addobj.ProductName = productNameComboBox.Text.Trim();
                addobj.ProductId = Convert.ToInt32(productNameComboBox.SelectedValue.ToString());
                addobj.ProductQuentity = Convert.ToDecimal(qtyTextBox.Text.ToString());
                addobj.ProductUnit = unitNameTextBox.Text.ToString();
                addobj.ProductRemark = remarkTextBox.Text.Trim();

                items.Add(addobj);
                orderListDataGridView.DataSource = null;
                orderListDataGridView.DataSource = items;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var item in items)
                {
                    ProductOrdered product = new ProductOrdered();
                    product.ProductOrderdate = item.RequestDate;
                    product.ProductOrderdeliverydate = item.DeliveryDate;
                    product.OrderQty = item.ProductQuentity;
                    product.ProductOrderRemark = item.ProductRemark;
                    product.ProductOrderNumber = item.OrderNumber;
                    product.ProductId = item.ProductId;
                    product.IsDelivered = false;
                    product.IsRawDelivered = false;
                    product.IsRawRequested = false;
                    service.ProductOrderedService.Save(product);
                }
                service.UnitOfWork.Commit();
                msgLabelValidation.ForeColor = Color.Green;
                msgLabelSuccess.Text = "যুক্ত সম্পূর্ণ হয়েছে............!";
                orderListDataGridView.DataSource = null;
            }
            catch (Exception ex)
            {
                msgLabelValidation.ForeColor = Color.Red;
                msgLabelSuccess.Text = "যুক্ত সম্পূর্ণ হয়নি............!";
            }
        }
    }
}
