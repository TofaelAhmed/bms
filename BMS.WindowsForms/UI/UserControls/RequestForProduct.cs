﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class RequestForProduct : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public RequestForProduct()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(display);
            BL.Start();
            GridviewDesign.Instance(orderListDataGridView);
        }
        public void display()
        {

            var data = service.ProductOrderedService.PendingOrdered();
                var modified = data.Select(x => new
                {
                    ProductOrderNumber = x.ProductOrderNumber,
                    ProductOrderdate = x.ProductOrderdate,
                    ProductOrderdeliverydate = x.ProductOrderdeliverydate,
                    ProductName = x.ProductDetailes.ProductName,
                    OrderQty = x.OrderQty,
                    UnitName = x.ProductDetailes.Units.UnitName,
                    ProductOrderRemark = x.ProductOrderRemark
                }).ToList();
                orderListDataGridView.DataSource = modified;
        }
        private void RequestForProduct_Load(object sender, EventArgs e)
        {
            //int a = productOrderGroupBox.Width - productOrderGroupBox..Width;
            //int b = productOrderGroupBox.Height - loderInabePictureBox.Image.Height;
            //Padding p = new System.Windows.Forms.Padding();
            //p.Left = a / 2;
            //p.Top = b / 2;
            //productOrderGroupBox.Padding = p;
        }

        private void Search_Click(object sender, EventArgs e)
        {

        }
    }
}
