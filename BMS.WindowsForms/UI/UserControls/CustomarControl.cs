﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class CustomarControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public CustomarControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(displayDrideview);
            BL.Start();
        }
        private void CustomarControl_Load(object sender, EventArgs e)
        {
            GridviewDesign.Instance(customarControlDataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            displayDrideview();
        }
        private void SaveCustomer_Click(object sender, EventArgs e)
        {
            Customar obj = new Customar();
            obj.CustomarName = customerNameTextBox.Text.Trim();
            obj.CustomarContracNo = customerMobileTextBox.Text.Trim();
            obj.CustomerNid = customarNIDTextBox.Text.Trim();
            obj.CustomarAddress = customerAddressTextBox.Text.Trim();

            service.CustomerService.Save(obj);
            service.UnitOfWork.Commit();
            MessageBox.Show("Sucess");
            displayDrideview();
        }
        void displayDrideview()
        {
            customarControlDataGridView.AutoGenerateColumns = false;
            customarControlDataGridView.DataSource = service.CustomerService.GetCustomars();
        }
    }
}
