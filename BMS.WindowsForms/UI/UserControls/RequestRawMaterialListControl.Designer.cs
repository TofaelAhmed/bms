﻿namespace BMS.WindowsForms.UI.UserControls
{
    partial class RequestRawMaterialListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.inputRawlabel = new System.Windows.Forms.Label();
            this.deleteButton = new System.Windows.Forms.Button();
            this.productionComboBox = new System.Windows.Forms.ComboBox();
            this.prodactionDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.productionRemarkTextBox = new System.Windows.Forms.TextBox();
            this.Save = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.qtyTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.productInfodataGridView = new System.Windows.Forms.DataGridView();
            this.RequestRawMaterialDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RawMaterId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RequestRawMaterialQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RequestRawMaterialRemark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productInfodataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(853, 488);
            this.panel1.TabIndex = 5;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.inputRawlabel);
            this.splitContainer1.Panel1.Controls.Add(this.deleteButton);
            this.splitContainer1.Panel1.Controls.Add(this.productionComboBox);
            this.splitContainer1.Panel1.Controls.Add(this.prodactionDateTimePicker);
            this.splitContainer1.Panel1.Controls.Add(this.productionRemarkTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.Save);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.qtyTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.productInfodataGridView);
            this.splitContainer1.Size = new System.Drawing.Size(853, 488);
            this.splitContainer1.SplitterDistance = 184;
            this.splitContainer1.TabIndex = 0;
            // 
            // inputRawlabel
            // 
            this.inputRawlabel.AutoSize = true;
            this.inputRawlabel.Location = new System.Drawing.Point(15, 312);
            this.inputRawlabel.Name = "inputRawlabel";
            this.inputRawlabel.Size = new System.Drawing.Size(0, 13);
            this.inputRawlabel.TabIndex = 20;
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(27)))), ((int)(((byte)(76)))));
            this.deleteButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteButton.FlatAppearance.BorderSize = 0;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deleteButton.Location = new System.Drawing.Point(47, 271);
            this.deleteButton.Margin = new System.Windows.Forms.Padding(0);
            this.deleteButton.MaximumSize = new System.Drawing.Size(68, 22);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Padding = new System.Windows.Forms.Padding(2);
            this.deleteButton.Size = new System.Drawing.Size(43, 19);
            this.deleteButton.TabIndex = 6;
            this.deleteButton.Text = "মুছুন";
            this.deleteButton.UseVisualStyleBackColor = false;
            // 
            // productionComboBox
            // 
            this.productionComboBox.FormattingEnabled = true;
            this.productionComboBox.Location = new System.Drawing.Point(9, 88);
            this.productionComboBox.Name = "productionComboBox";
            this.productionComboBox.Size = new System.Drawing.Size(173, 21);
            this.productionComboBox.TabIndex = 3;
            // 
            // prodactionDateTimePicker
            // 
            this.prodactionDateTimePicker.Location = new System.Drawing.Point(8, 41);
            this.prodactionDateTimePicker.Name = "prodactionDateTimePicker";
            this.prodactionDateTimePicker.Size = new System.Drawing.Size(174, 20);
            this.prodactionDateTimePicker.TabIndex = 1;
            // 
            // productionRemarkTextBox
            // 
            this.productionRemarkTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.productionRemarkTextBox.Location = new System.Drawing.Point(6, 188);
            this.productionRemarkTextBox.Multiline = true;
            this.productionRemarkTextBox.Name = "productionRemarkTextBox";
            this.productionRemarkTextBox.Size = new System.Drawing.Size(169, 64);
            this.productionRemarkTextBox.TabIndex = 4;
            // 
            // Save
            // 
            this.Save.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Save.Location = new System.Drawing.Point(102, 270);
            this.Save.MaximumSize = new System.Drawing.Size(73, 23);
            this.Save.MinimumSize = new System.Drawing.Size(73, 23);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(73, 23);
            this.Save.TabIndex = 5;
            this.Save.Text = "যুক্ত করুণ";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "মন্তব্য";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "পরিমান";
            // 
            // qtyTextBox
            // 
            this.qtyTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qtyTextBox.Location = new System.Drawing.Point(8, 138);
            this.qtyTextBox.Name = "qtyTextBox";
            this.qtyTextBox.Size = new System.Drawing.Size(166, 20);
            this.qtyTextBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "কাঁচামালের নাম";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "তারিখ";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(184, 0);
            this.panel2.TabIndex = 0;
            // 
            // productInfodataGridView
            // 
            this.productInfodataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.productInfodataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.productInfodataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.productInfodataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productInfodataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RequestRawMaterialDate,
            this.RawMaterId,
            this.RequestRawMaterialQty,
            this.RequestRawMaterialRemark});
            this.productInfodataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productInfodataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.productInfodataGridView.Location = new System.Drawing.Point(0, 0);
            this.productInfodataGridView.Name = "productInfodataGridView";
            this.productInfodataGridView.Size = new System.Drawing.Size(665, 488);
            this.productInfodataGridView.TabIndex = 0;
            // 
            // RequestRawMaterialDate
            // 
            this.RequestRawMaterialDate.DataPropertyName = "RequestRawMaterialDate";
            this.RequestRawMaterialDate.HeaderText = "উৎপাদনের তারিখ";
            this.RequestRawMaterialDate.Name = "RequestRawMaterialDate";
            // 
            // RawMaterId
            // 
            this.RawMaterId.DataPropertyName = "RawMaterialName";
            this.RawMaterId.HeaderText = "কাঁচামালের নাম";
            this.RawMaterId.Name = "RawMaterId";
            // 
            // RequestRawMaterialQty
            // 
            this.RequestRawMaterialQty.DataPropertyName = "RequestRawMaterialQty";
            this.RequestRawMaterialQty.HeaderText = "পরিমান";
            this.RequestRawMaterialQty.Name = "RequestRawMaterialQty";
            // 
            // RequestRawMaterialRemark
            // 
            this.RequestRawMaterialRemark.DataPropertyName = "RequestRawMaterialRemark";
            this.RequestRawMaterialRemark.HeaderText = "মন্তব্য";
            this.RequestRawMaterialRemark.Name = "RequestRawMaterialRemark";
            // 
            // RequestRawMaterialListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "RequestRawMaterialListControl";
            this.Size = new System.Drawing.Size(853, 488);
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.productInfodataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.ComboBox productionComboBox;
        private System.Windows.Forms.DateTimePicker prodactionDateTimePicker;
        private System.Windows.Forms.TextBox productionRemarkTextBox;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox qtyTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView productInfodataGridView;
        private System.Windows.Forms.Label inputRawlabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequestRawMaterialDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RawMaterId;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequestRawMaterialQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequestRawMaterialRemark;
    }
}
