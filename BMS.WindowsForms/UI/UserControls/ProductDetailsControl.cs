﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;
using BMS.Services.BMS;
using System.Threading;
using BMS.Entities.ViewModel.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class ProductDetailsControl : UserControl
    {
        public bool islodingSuccess = false;
        ServiceAccess service = new ServiceAccess();
        List<ProductDetailsVM> items = new List<ProductDetailsVM>();
        ProductDetailsVM additem = new ProductDetailsVM();
        public ProductDetailsControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(productComboboxDataLoad);
            BL.Start();
            GridviewDesign.Instance(productDetailsDataGridView);
            productDetailsDataGridView.AutoGenerateColumns = false;
            pawMatarialComboboxDataLoad();
        }
        void textboxclear()
        {
            rawMaterialComboBox.ResetText();
            quantityTextBox.Clear();
            rawMaterialunitTextBox.Clear();
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
            textboxclear();
        }
       
        private void procomdaload()
        {
            unitTextBox.Text = "";
            if (productNameComboBox.SelectedIndex < 0) return;
            var id = productNameComboBox.SelectedValue.ToString();
            decimal num;
            var isValidNum = decimal.TryParse(id, out num);
            if (isValidNum)
            {
                var rawMaterialid = Convert.ToInt32(productNameComboBox.SelectedValue);
                var itemes = service.RawMaterialService.GetRawMaterialById(rawMaterialid);
                var unit = service.UnitService.GetUnitsById(itemes.UnitId);
                unitTextBox.Text = unit.UnitName;
            }
        }
        public void productComboboxDataLoad()
        {
            var item = service.ProductDetailsService.GetProductDetails();
            var modified = item.Select(x => new { ProductName = x.ProductName, ProductId = x.ProductId }).ToList();
            productNameComboBox.DataSource = new BindingSource(modified, null);
            productNameComboBox.DisplayMember = "ProductName";
            productNameComboBox.ValueMember = "ProductId";
            productNameComboBox.SelectedIndex = -1;
        }
        public void pawMatarialComboboxDataLoad()
        {
            var item = service.RawMaterialService.GetRawMaterials();
            var modified = item.Select(x => new { RawMaterialName = x.RawMaterialName, RawMaterialId = x.RawMaterialId }).ToList();
            rawMaterialComboBox.DataSource = new BindingSource(modified, null);
            rawMaterialComboBox.DisplayMember = "RawMaterialName";
            rawMaterialComboBox.ValueMember = "RawMaterialId";
            rawMaterialComboBox.SelectedIndex = -1;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            ProductEntry pe = new ProductEntry();
            pe.Show();
        }

        private void rawMaterialComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            rawMaterialunitTextBox.Text = "";
            if (rawMaterialComboBox.SelectedIndex < 0) return;
            var id = rawMaterialComboBox.SelectedValue.ToString();
            decimal num;
            var isValidNum = decimal.TryParse(id, out num);
            if (isValidNum)
            {
                var rawMaterialid = Convert.ToInt32(rawMaterialComboBox.SelectedValue);
                var item = service.RawMaterialService.GetRawMaterialById(rawMaterialid);
                var unit = service.UnitService.GetUnitsById(item.UnitId);
                rawMaterialunitTextBox.Text = unit.UnitName;
            }
        }
       
        private void display(int productId)
        {
            var data = service.ProductRawmaterialMappingService.GetProductRawmaterialMappingsById(productId);
            var modified = data.Select(x => new
            {
                ProductRawmaterialMappingId = x.ProductWithRawMappingId,
                ProductName = x.ProductDetails.ProductName,
                RawMaterialName = x.RawMaterials.RawMaterialName,
                Quantity = x.Quentity,
                UnitName = x.RawMaterials.Units.UnitName
            }).ToList();
            productDetailsDataGridView.DataSource = modified;
            productDetailsDataGridView.AutoGenerateColumns = false;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if(IsValid())
            {
                ProductDetailsVM additem = new ProductDetailsVM();
                additem.ProductId = Convert.ToInt32(productNameComboBox.SelectedValue.ToString());
                additem.ProductName = productNameComboBox.Text.ToString();
                additem.RawatarialId = Convert.ToInt32(rawMaterialComboBox.SelectedValue.ToString());
                additem.RawatarialName = rawMaterialComboBox.Text.ToString();
                additem.Quantity = Convert.ToDecimal(quantityTextBox.Text.ToString());
                additem.UnitName = rawMaterialunitTextBox.Text.ToString();

                items.Add(additem);
                productDetailsDataGridView.DataSource = null;
                productDetailsDataGridView.DataSource = items;
                textboxclear();
            }
        }
        private void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var item in items)
                {
                    ProductRawmaterialMapping product = new ProductRawmaterialMapping();
                    product.ProductId = item.ProductId;
                    product.RawMaterialId = item.RawatarialId;
                    product.Quentity = item.Quantity;
                    service.ProductRawmaterialMappingService.Save(product);
                }
                service.UnitOfWork.Commit();
                inputRawlabel.ForeColor = Color.Green;
                inputRawlabel.Text = "যুক্ত সম্পূর্ণ হয়েছে............!";
                productDetailsDataGridView.DataSource = null;
            }
            catch (Exception ex)
            {
                inputRawlabel.ForeColor = Color.Red;
                inputRawlabel.Text = "যুক্ত সম্পূর্ণ হয়নি............!";
            }
        }

        private void productNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            unitTextBox.Text = "";
            if (productNameComboBox.SelectedIndex < 0) return;
            var id = productNameComboBox.SelectedValue.ToString();
            decimal num;
            var isValidNum = decimal.TryParse(id, out num);
            if (isValidNum)
            {
                var productid = Convert.ToInt32(productNameComboBox.SelectedValue);
                var item = service.ProductDetailsService.GetProductDetailById(productid);
                var unit = service.UnitService.GetUnitsById(item.UnitId);
                unitTextBox.Text = unit.UnitName;

                display(productid);
            }
        }

        private void productDetailsDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = productDetailsDataGridView.Rows[e.RowIndex];
                productNameComboBox.Text = row.Cells[0].Value.ToString();
                rawMaterialComboBox.Text = row.Cells[1].Value.ToString();
                quantityTextBox.Text = row.Cells[2].Value.ToString();
            }
            submitButton.Text = "পরিবর্তন করুণ";
        }

        private bool IsValid()
        {
            inputRawlabel.Text = "";
            inputRawlabel.ForeColor = Color.Red;

            if (!productNameComboBox.Text.IsEmpty() || productNameComboBox.SelectedIndex < 0)
            {
                inputRawlabel.Text = " পণ্যের নাম নির্বাচন করুন ";
                return false;
            }
            if (!rawMaterialComboBox.Text.IsEmpty() || rawMaterialComboBox.SelectedIndex < 0)
            {
                inputRawlabel.Text = " কাঁচামালের নাম নির্বাচন করুন ";
                return false;
            }
            if (!quantityTextBox.Text.IsEmpty())
            {
                inputRawlabel.Text = " পরিমান লিখুন ";
                return false;
            }
            inputRawlabel.ForeColor = Color.Black;
            return true;
        }
        
        private void quantityTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 2404)
            {
                ch = '.';
                e.KeyChar = '.';
            }
            if (!ch.IsValidDigit())
            {
                inputRawlabel.ForeColor = Color.Red;
                e.Handled = true;
                inputRawlabel.Text = @" সঠিক সংখ্যা লিখুন";
            }
            else
            {
                if (!ch.ToString().IsValidNumber() && ch != 8 && ch != 46)
                {
                    e.KeyChar = (char)('0' + e.KeyChar - '\u09E6');
                }
                inputRawlabel.Text = "";
            }
        }

    }
}