﻿namespace BMS.WindowsForms.UI.UserControls
{
    partial class RequestRawMaterialControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.productOrderGroupBox = new System.Windows.Forms.GroupBox();
            this.RequestRawMatarailDataGridView = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.messagesLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.submitButton = new System.Windows.Forms.Button();
            this.msgLabelValidation = new System.Windows.Forms.Label();
            this.ProductOrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductOrderdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductOrderdeliverydate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductOrderRemark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsRawRequested = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.productOrderGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RequestRawMatarailDataGridView)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(326, 57);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "খোঁজ করুণ";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(95, 60);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(135, 20);
            this.dateTimePicker2.TabIndex = 14;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(95, 18);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(135, 20);
            this.dateTimePicker1.TabIndex = 14;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(326, 17);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(186, 21);
            this.comboBox1.TabIndex = 13;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(238, 18);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(80, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = " অর্ডার নাম্বার :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "শেষের তারিখ :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "শুরুর তারিখ :";
            // 
            // productOrderGroupBox
            // 
            this.productOrderGroupBox.Controls.Add(this.button1);
            this.productOrderGroupBox.Controls.Add(this.dateTimePicker2);
            this.productOrderGroupBox.Controls.Add(this.dateTimePicker1);
            this.productOrderGroupBox.Controls.Add(this.comboBox1);
            this.productOrderGroupBox.Controls.Add(this.label49);
            this.productOrderGroupBox.Controls.Add(this.label2);
            this.productOrderGroupBox.Controls.Add(this.label1);
            this.productOrderGroupBox.Location = new System.Drawing.Point(8, 2);
            this.productOrderGroupBox.Name = "productOrderGroupBox";
            this.productOrderGroupBox.Size = new System.Drawing.Size(520, 91);
            this.productOrderGroupBox.TabIndex = 2;
            this.productOrderGroupBox.TabStop = false;
            // 
            // RequestRawMatarailDataGridView
            // 
            this.RequestRawMatarailDataGridView.AllowUserToAddRows = false;
            this.RequestRawMatarailDataGridView.AllowUserToDeleteRows = false;
            this.RequestRawMatarailDataGridView.AllowUserToOrderColumns = true;
            this.RequestRawMatarailDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RequestRawMatarailDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RequestRawMatarailDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.RequestRawMatarailDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.RequestRawMatarailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RequestRawMatarailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductOrderNumber,
            this.ProductOrderdate,
            this.ProductOrderdeliverydate,
            this.ProductName,
            this.OrderQty,
            this.UnitName,
            this.ProductOrderRemark,
            this.IsRawRequested});
            this.RequestRawMatarailDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.RequestRawMatarailDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.RequestRawMatarailDataGridView.Location = new System.Drawing.Point(1, -2);
            this.RequestRawMatarailDataGridView.Name = "RequestRawMatarailDataGridView";
            this.RequestRawMatarailDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RequestRawMatarailDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.RequestRawMatarailDataGridView.Size = new System.Drawing.Size(1003, 246);
            this.RequestRawMatarailDataGridView.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.RequestRawMatarailDataGridView);
            this.panel1.Location = new System.Drawing.Point(0, 133);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 248);
            this.panel1.TabIndex = 4;
            // 
            // messagesLabel
            // 
            this.messagesLabel.AutoSize = true;
            this.messagesLabel.ForeColor = System.Drawing.Color.Green;
            this.messagesLabel.Location = new System.Drawing.Point(447, 108);
            this.messagesLabel.Name = "messagesLabel";
            this.messagesLabel.Size = new System.Drawing.Size(0, 13);
            this.messagesLabel.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.messagesLabel);
            this.panel2.Controls.Add(this.productOrderGroupBox);
            this.panel2.Location = new System.Drawing.Point(0, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1008, 131);
            this.panel2.TabIndex = 5;
            // 
            // submitButton
            // 
            this.submitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.submitButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.submitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(27)))), ((int)(((byte)(76)))));
            this.submitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.submitButton.FlatAppearance.BorderSize = 0;
            this.submitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.submitButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.submitButton.Location = new System.Drawing.Point(927, 384);
            this.submitButton.Margin = new System.Windows.Forms.Padding(0);
            this.submitButton.MaximumSize = new System.Drawing.Size(80, 25);
            this.submitButton.Name = "submitButton";
            this.submitButton.Padding = new System.Windows.Forms.Padding(2);
            this.submitButton.Size = new System.Drawing.Size(78, 25);
            this.submitButton.TabIndex = 6;
            this.submitButton.Text = "দাখিল করুণ";
            this.submitButton.UseVisualStyleBackColor = false;
            this.submitButton.Click += new System.EventHandler(this.RequestRawMatarailsubmitButton_Click);
            // 
            // msgLabelValidation
            // 
            this.msgLabelValidation.AutoSize = true;
            this.msgLabelValidation.Location = new System.Drawing.Point(62, 396);
            this.msgLabelValidation.Name = "msgLabelValidation";
            this.msgLabelValidation.Size = new System.Drawing.Size(14, 13);
            this.msgLabelValidation.TabIndex = 7;
            this.msgLabelValidation.Text = "#";
            // 
            // ProductOrderNumber
            // 
            this.ProductOrderNumber.DataPropertyName = "ProductOrderNumber";
            this.ProductOrderNumber.HeaderText = "অ.নাং. ";
            this.ProductOrderNumber.Name = "ProductOrderNumber";
            this.ProductOrderNumber.ReadOnly = true;
            // 
            // ProductOrderdate
            // 
            this.ProductOrderdate.DataPropertyName = "ProductOrderdate";
            this.ProductOrderdate.HeaderText = "অনুরোধের তারিখ";
            this.ProductOrderdate.Name = "ProductOrderdate";
            this.ProductOrderdate.ReadOnly = true;
            // 
            // ProductOrderdeliverydate
            // 
            this.ProductOrderdeliverydate.DataPropertyName = "ProductOrderdeliverydate";
            this.ProductOrderdeliverydate.HeaderText = "হস্তাতরের তারিখ";
            this.ProductOrderdeliverydate.Name = "ProductOrderdeliverydate";
            this.ProductOrderdeliverydate.ReadOnly = true;
            // 
            // ProductName
            // 
            this.ProductName.DataPropertyName = "ProductName";
            this.ProductName.HeaderText = "পণ্যের নাম";
            this.ProductName.Name = "ProductName";
            this.ProductName.ReadOnly = true;
            // 
            // OrderQty
            // 
            this.OrderQty.DataPropertyName = "OrderQty";
            this.OrderQty.HeaderText = "পরিমান";
            this.OrderQty.Name = "OrderQty";
            this.OrderQty.ReadOnly = true;
            // 
            // UnitName
            // 
            this.UnitName.DataPropertyName = "UnitName";
            this.UnitName.HeaderText = "একক";
            this.UnitName.Name = "UnitName";
            this.UnitName.ReadOnly = true;
            // 
            // ProductOrderRemark
            // 
            this.ProductOrderRemark.DataPropertyName = "ProductOrderRemark";
            this.ProductOrderRemark.HeaderText = "মন্তব্য";
            this.ProductOrderRemark.Name = "ProductOrderRemark";
            this.ProductOrderRemark.ReadOnly = true;
            // 
            // IsRawRequested
            // 
            this.IsRawRequested.HeaderText = "কাঁচামালের জন্য অনু.";
            this.IsRawRequested.Name = "IsRawRequested";
            this.IsRawRequested.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsRawRequested.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // RequestRawMaterialControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.msgLabelValidation);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "RequestRawMaterialControl";
            this.Size = new System.Drawing.Size(1008, 685);
            this.productOrderGroupBox.ResumeLayout(false);
            this.productOrderGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RequestRawMatarailDataGridView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox productOrderGroupBox;
        private System.Windows.Forms.DataGridView RequestRawMatarailDataGridView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label messagesLabel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label msgLabelValidation;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductOrderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductOrderdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductOrderdeliverydate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductOrderRemark;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsRawRequested;
    }
}
