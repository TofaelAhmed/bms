﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class PricePaymentControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public PricePaymentControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(display);
            BL.Start();
        }

        private void PricePaymentInformation_Load(object sender, EventArgs e)
        {
            GridviewDesign.Instance(pricePayInfoDataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            display();
        }

        private void Save_Click(object sender, EventArgs e)
        {
      

           // service.PricePaymentServise.Save(obj);
            service.UnitOfWork.Commit();
            display();
        }
        void display()
        {
            pricePayInfoDataGridView.AutoGenerateColumns = false;
            //pricePayInfoDataGridView.DataSource = service.PricePaymentServise.GetPricePayments();
        }
    }
}
