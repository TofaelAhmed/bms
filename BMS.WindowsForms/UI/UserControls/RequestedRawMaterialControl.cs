﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.WindowsForms.UI.ReportUi;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class RequestedRawMaterialControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public RequestedRawMaterialControl()
        {
            InitializeComponent();
            GridviewDesign.Instance(RequestedRawMatarailDataGridView);
            RequestedRawMatarailDataGridView.AutoGenerateColumns = false;
        }

        private void RequestedRawMaterialControl_Load(object sender, EventArgs e)
        {
            var dt = service.RequestRawMaterialService.GetRequestedRawMaterials();
            RequestedRawMatarailDataGridView.DataSource = dt;
        }

        private void printButton_Click(object sender, EventArgs e)
        {
            var dt = service.RequestRawMaterialService.GetRequestedRawMaterials();
            RequestedRawMatrials show = new RequestedRawMatrials(dt);
            show.Show();

        }
    }
}
