﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class ProductControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public ProductControl()
        {
            InitializeComponent();
        }

        private void ProductControl_Load(object sender, EventArgs e)
        {
            GridviewDesign.Instance(productControldataGridView1);
            GridviewDesign.Instance(productControldataGridView2);
            GridviewDesign.Instance(productControldataGridView3);
        }

        private void ProductControl_Load_1(object sender, EventArgs e)
        {
            
        }
    }
}
