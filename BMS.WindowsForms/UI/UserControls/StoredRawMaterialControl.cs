﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class StoredRawMaterialControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public StoredRawMaterialControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(comboboxdataload);
            BL.Start();
            GridviewDesign.Instance(storedRawMaterialInformationdataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            display();
        }

        private void StoredRawMaterialInformation_Load(object sender, EventArgs e)
        {
            
        }

        private void Save_Click(object sender, EventArgs e)
        {
            StoredRawMaterial obj = new StoredRawMaterial();
            obj.StoredRawMaterialName = storeRawMatarialComboBox.SelectedItem.ToString();
            obj.StoredRawMaterialUnit = storeRawMaterialUnitTextBox.Text.Trim();

            service.StoredRawMaterialServise.Save(obj);
            service.UnitOfWork.Commit();
            display();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            storeRawMatarialComboBox.ResetText();
            storeRawMaterialUnitTextBox.Clear();
        }
        void display()
        {
            storedRawMaterialInformationdataGridView.AutoGenerateColumns = false;
            storedRawMaterialInformationdataGridView.DataSource = service.StoredRawMaterialServise.GetStoredRawMaterials();
        }
        void comboboxdataload()
        {
            storeRawMatarialComboBox.DataSource = new BindingSource(service.RawMaterialService.GetRawMaterials(), null);
            storeRawMatarialComboBox.DisplayMember = "RawMaterialName";
            storeRawMatarialComboBox.ValueMember = "RawMaterialId";
            storeRawMatarialComboBox.SelectedIndex = -1;
        }
    }
}
