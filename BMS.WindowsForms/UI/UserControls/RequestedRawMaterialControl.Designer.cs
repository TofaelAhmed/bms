﻿namespace BMS.WindowsForms.UI.UserControls
{
    partial class RequestedRawMaterialControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RequestedRawMatarailDataGridView = new System.Windows.Forms.DataGridView();
            this.RawMaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quntity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.printButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.RequestedRawMatarailDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // RequestedRawMatarailDataGridView
            // 
            this.RequestedRawMatarailDataGridView.AllowUserToAddRows = false;
            this.RequestedRawMatarailDataGridView.AllowUserToDeleteRows = false;
            this.RequestedRawMatarailDataGridView.AllowUserToOrderColumns = true;
            this.RequestedRawMatarailDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RequestedRawMatarailDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RequestedRawMatarailDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.RequestedRawMatarailDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.RequestedRawMatarailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RequestedRawMatarailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RawMaterialName,
            this.Quntity,
            this.UnitName});
            this.RequestedRawMatarailDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.RequestedRawMatarailDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.RequestedRawMatarailDataGridView.Location = new System.Drawing.Point(0, 55);
            this.RequestedRawMatarailDataGridView.Name = "RequestedRawMatarailDataGridView";
            this.RequestedRawMatarailDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RequestedRawMatarailDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.RequestedRawMatarailDataGridView.Size = new System.Drawing.Size(1009, 713);
            this.RequestedRawMatarailDataGridView.TabIndex = 12;
            // 
            // RawMaterialName
            // 
            this.RawMaterialName.DataPropertyName = "RawMaterialName";
            this.RawMaterialName.HeaderText = "কাঁচামালের নাম";
            this.RawMaterialName.Name = "RawMaterialName";
            this.RawMaterialName.ReadOnly = true;
            // 
            // Quntity
            // 
            this.Quntity.DataPropertyName = "Quntity";
            this.Quntity.HeaderText = "পরিমান";
            this.Quntity.Name = "Quntity";
            this.Quntity.ReadOnly = true;
            // 
            // UnitName
            // 
            this.UnitName.DataPropertyName = "UnitName";
            this.UnitName.HeaderText = "একক";
            this.UnitName.Name = "UnitName";
            this.UnitName.ReadOnly = true;
            // 
            // printButton
            // 
            this.printButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.printButton.BackColor = System.Drawing.SystemColors.HotTrack;
            this.printButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.printButton.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printButton.ForeColor = System.Drawing.Color.White;
            this.printButton.Location = new System.Drawing.Point(914, 14);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(82, 35);
            this.printButton.TabIndex = 13;
            this.printButton.Text = "Print";
            this.printButton.UseVisualStyleBackColor = false;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // RequestedRawMaterialControl
            // 
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.RequestedRawMatarailDataGridView);
            this.Name = "RequestedRawMaterialControl";
            this.Size = new System.Drawing.Size(1009, 768);
            this.Load += new System.EventHandler(this.RequestedRawMaterialControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RequestedRawMatarailDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView RequestedRawMatarailDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn RawMaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quntity;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitName;
        private System.Windows.Forms.Button printButton;
    }
}
