﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class SalaryPaymentControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public SalaryPaymentControl()
        {
            InitializeComponent();
        }

        private void SalaryPaymentInformation_Load(object sender, EventArgs e)
        {
            GridviewDesign.Instance(salaryPaymentInformationdataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            display();
            comboboxdataload();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            DateTime dtime;
            dtime = alaryPaymentDateDateTimePicker.Value.Date;
            SalaryPayment obj = new SalaryPayment();
            obj.SalaryPaymentEmpName = employeeComboBox.SelectedItem.ToString();
            obj.SalaryPaymentDate = dtime;
            obj.SalaryPaymentAmount = Convert.ToInt64(salaryPayAmountTextBox.Text.Trim());

            service.SalaryPaymentServise.Save(obj);
            service.UnitOfWork.Commit();
            display();
            
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            employeeComboBox.ResetText();
            alaryPaymentDateDateTimePicker.ResetText();
            salaryPayAmountTextBox.Clear();
        }
        void display()
        {
            salaryPaymentInformationdataGridView.AutoGenerateColumns = false;
            salaryPaymentInformationdataGridView.DataSource = service.SalaryPaymentServise.GetSalaryPayments();
        }
        void comboboxdataload()
        {
            employeeComboBox.DataSource = new BindingSource(service.EmployeeService.GetEmployees(), null);
            employeeComboBox.DisplayMember = "EmployeeName";
            employeeComboBox.ValueMember = "EmployeeId";
            employeeComboBox.SelectedIndex = -1;
        }

    }
}