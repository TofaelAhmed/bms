﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class RequestRawMaterialControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public RequestRawMaterialControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(Display);
            BL.Start();
            GridviewDesign.Instance(RequestRawMatarailDataGridView);
        }
        void Display()
        {
            var data = service.ProductOrderedService.PendingOrdered();
            var modified = data.Select(x => new
            {
                ProductOrderId = x.ProductOrderId,
                ProductOrderNumber = x.ProductOrderNumber,
                ProductOrderdate = x.ProductOrderdate,
                ProductOrderdeliverydate = x.ProductOrderdeliverydate,
                ProductName = x.ProductDetailes.ProductName,
                OrderQty = x.OrderQty,
                UnitName = x.ProductDetailes.Units.UnitName,
                ProductOrderRemark = x.ProductOrderRemark
            }).ToList();
            RequestRawMatarailDataGridView.DataSource = modified;
            
            RequestRawMatarailDataGridView.AutoGenerateColumns = false;
            var ProductOrderId = RequestRawMatarailDataGridView.Columns["ProductOrderId"];
            if (ProductOrderId != null)
                ProductOrderId.Visible = false;
        }
        private void RequestRawMatarailsubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in RequestRawMatarailDataGridView.Rows)
                {
                    ProductOrdered product = new ProductOrdered();
                    product.ProductOrderId = Convert.ToInt32(row.Cells["ProductOrderId"].Value);
                    product.IsRawRequested = Convert.ToBoolean(row.Cells["IsRawRequested"].Value);
                    if (product != null && product.IsRawRequested==true)
                    {
                        var item = service.ProductOrderedService.GetById(product.ProductOrderId);
                        item.IsRawRequested = true;
                        service.ProductOrderedService.Edit(item);
                        service.UnitOfWork.Commit();
                        msgLabelValidation.ForeColor = Color.Green;
                        msgLabelValidation.Text = "অনুরোধ সম্পূর্ণ হয়েছে............!";
                    }
                }
            }
            catch (Exception ex)
            {
                msgLabelValidation.ForeColor = Color.Red;
                msgLabelValidation.Text = "অনুরোধ সম্পূর্ণ হয়নি............!";
            }
        }
    }
}