﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class RawMaterialBuyControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public RawMaterialBuyControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(comboboxdataload);
            BL.Start();
            display();
            commitonTextBox.Text = "0";
        }
        private int initialvalue()
        {
            int num = 0;
            commitonTextBox.Text = num.ToString();
            return num;
        }
        void display()
        {
            virtualDataGridView.AutoGenerateColumns = false;
            GridviewDesign.Instance(virtualDataGridView);
        }
        public void comboboxdataload()
        {
            var item = service.RawMaterialService.GetRawMaterials();
            var modified = item.Select(x => new { RawMaterialName = x.RawMaterialName, RawMaterialId = x.RawMaterialId }).ToList();
            rawMatarialNameComboBox.DataSource = new BindingSource(modified, null);
            rawMatarialNameComboBox.DisplayMember = "RawMaterialName";
            rawMatarialNameComboBox.ValueMember = "RawMaterialId";
            rawMatarialNameComboBox.SelectedIndex = -1;
        }
        List<VirtualModel> items = new List<VirtualModel>();
        private void addButton_Click_1(object sender, EventArgs e)
        {
            if (IsValid())
            {
                DateTime dtime;
                dtime = rawMatariaDateTimePicker.Value.Date;
                VirtualModel additem = new VirtualModel();
                additem.Date = dtime;
                additem.RawMatarialName = rawMatarialNameComboBox.Text.ToString();
                additem.Unit = unitTextBox.Text.ToString();
                additem.Qty = Convert.ToDecimal(qtyTextBox.Text.ToString());
                additem.Price = Convert.ToDecimal(priceTextBox.Text.ToString());
                additem.Total = Convert.ToDecimal(totalTextBox.Text.ToString());
                additem.Comition = Convert.ToInt32(initialvalue());

                items.Add(additem);
                virtualDataGridView.DataSource = null;
                virtualDataGridView.DataSource = items;

                totalTextBox.Text = items.Sum(x => x.Total).ToString();
                subTotalTextBox.Text = totalTextBox.Text.ToString();
                totalCommitionTextBox.Text = items.Sum(x => x.Comition).ToString();
                var subTota = Convert.ToDecimal(totalTextBox.Text.ToString());
                var totalCommition = Convert.ToDecimal(totalCommitionTextBox.Text.ToString());
                decimal grendTotal = subTota - totalCommition;
                grendTotalTextBox.Text = grendTotal.ToString();
            }
        }
        private void rawMatarialNameComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            unitTextBox.Text = "";
            if (rawMatarialNameComboBox.SelectedIndex < 0) return;
            var id = rawMatarialNameComboBox.SelectedValue.ToString();
            decimal num;
            var isValidNum = decimal.TryParse(id, out num);
            if (isValidNum)
            {
                var rawMaterialid = Convert.ToInt32(rawMatarialNameComboBox.SelectedValue);
                var item = service.RawMaterialService.GetRawMaterialById(rawMaterialid);
                var unit = service.UnitService.GetUnitsById(item.UnitId);
                unitTextBox.Text = unit.UnitName;
            }
        }
        private bool IsValid()
        {
            rawMatarialBuyValidationMessageLabel.Text = "";
            rawMatarialBuyValidationMessageLabel.ForeColor = Color.Red;
            if (!rawMatarialNameComboBox.Text.IsEmpty() || rawMatarialNameComboBox.SelectedIndex < 0)
            {
                rawMatarialBuyValidationMessageLabel.Text = " কাঁচামালের নাম লিখুন ";
                return false;
            }
            if (!qtyTextBox.Text.IsEmpty() || !qtyTextBox.Text.IsValidNumber())
            {
                rawMatarialBuyValidationMessageLabel.Text = " সঠিক পরিমান দিন ";
                return false;
            }
            if (!priceTextBox.Text.IsEmpty() || !priceTextBox.Text.IsValidNumber())
            {
                rawMatarialBuyValidationMessageLabel.Text = " সঠিক মূল্য দিন  ";
                return false;
            }
            rawMatarialBuyValidationMessageLabel.ForeColor = Color.Black;
            return true;
        }
        private void priceTextBox_TextChanged(object sender, EventArgs e)
        {
            if (IsValid())
            {
                var qty = Convert.ToDecimal(qtyTextBox.Text.Trim());
                var price = Convert.ToDecimal(priceTextBox.Text.Trim());
                var total = qty * price;
                totalTextBox.Text = total.ToString();
            }
        }
        private void qtyTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 2404)
            {
                ch = '.';
                e.KeyChar = '.';
            }
            if (!ch.IsValidDigit())
            {
                rawMatarialBuyValidationMessageLabel.ForeColor = Color.Red;
                e.Handled = true;
                rawMatarialBuyValidationMessageLabel.Text = @" সঠিক সংখ্যা লিখুন";
            }
            else
            {
                if (!ch.ToString().IsValidNumber() && ch != 8 && ch != 46)
                {
                    e.KeyChar = (char)('0' + e.KeyChar - '\u09E6');
                }
                rawMatarialBuyValidationMessageLabel.Text = "";
            }
        }

        private void submitButton_Click(object sender, EventArgs e)
        {

        }
    }
}
