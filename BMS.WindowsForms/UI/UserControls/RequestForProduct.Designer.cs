﻿namespace BMS.WindowsForms.UI.UserControls
{
    partial class RequestForProduct
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.orderListDataGridView = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.productOrderGroupBox = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.messagesLabel = new System.Windows.Forms.Label();
            this.ProductOrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductOrderdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductOrderdeliverydate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductOrderRemark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderListDataGridView)).BeginInit();
            this.panel2.SuspendLayout();
            this.productOrderGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.orderListDataGridView);
            this.panel1.Location = new System.Drawing.Point(0, 134);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1013, 313);
            this.panel1.TabIndex = 2;
            // 
            // orderListDataGridView
            // 
            this.orderListDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.orderListDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.orderListDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.orderListDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.orderListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductOrderNumber,
            this.ProductOrderdate,
            this.ProductOrderdeliverydate,
            this.ProductName,
            this.OrderQty,
            this.UnitName,
            this.ProductOrderRemark});
            this.orderListDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.orderListDataGridView.Location = new System.Drawing.Point(0, -1);
            this.orderListDataGridView.Name = "orderListDataGridView";
            this.orderListDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.orderListDataGridView.Size = new System.Drawing.Size(1012, 312);
            this.orderListDataGridView.TabIndex = 11;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.messagesLabel);
            this.panel2.Controls.Add(this.productOrderGroupBox);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1013, 131);
            this.panel2.TabIndex = 3;
            // 
            // productOrderGroupBox
            // 
            this.productOrderGroupBox.Controls.Add(this.button1);
            this.productOrderGroupBox.Controls.Add(this.dateTimePicker2);
            this.productOrderGroupBox.Controls.Add(this.dateTimePicker1);
            this.productOrderGroupBox.Controls.Add(this.comboBox1);
            this.productOrderGroupBox.Controls.Add(this.label49);
            this.productOrderGroupBox.Controls.Add(this.label2);
            this.productOrderGroupBox.Controls.Add(this.label1);
            this.productOrderGroupBox.Location = new System.Drawing.Point(137, 2);
            this.productOrderGroupBox.Name = "productOrderGroupBox";
            this.productOrderGroupBox.Size = new System.Drawing.Size(627, 99);
            this.productOrderGroupBox.TabIndex = 2;
            this.productOrderGroupBox.TabStop = false;
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(355, 62);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "খোঁজ করুণ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Search_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(105, 63);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(135, 20);
            this.dateTimePicker2.TabIndex = 14;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(105, 21);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(135, 20);
            this.dateTimePicker1.TabIndex = 14;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(355, 20);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(186, 21);
            this.comboBox1.TabIndex = 13;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(267, 21);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(80, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = " অর্ডার নাম্বার :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "শেষের তারিখ :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "শুরুর তারিখ :";
            // 
            // messagesLabel
            // 
            this.messagesLabel.AutoSize = true;
            this.messagesLabel.ForeColor = System.Drawing.Color.Green;
            this.messagesLabel.Location = new System.Drawing.Point(447, 108);
            this.messagesLabel.Name = "messagesLabel";
            this.messagesLabel.Size = new System.Drawing.Size(0, 13);
            this.messagesLabel.TabIndex = 13;
            // 
            // ProductOrderNumber
            // 
            this.ProductOrderNumber.DataPropertyName = "ProductOrderNumber";
            this.ProductOrderNumber.HeaderText = "অনুরধের লিস্ট";
            this.ProductOrderNumber.Name = "ProductOrderNumber";
            // 
            // ProductOrderdate
            // 
            this.ProductOrderdate.DataPropertyName = "ProductOrderdate";
            this.ProductOrderdate.HeaderText = "অনুরোধের তারিখ";
            this.ProductOrderdate.Name = "ProductOrderdate";
            // 
            // ProductOrderdeliverydate
            // 
            this.ProductOrderdeliverydate.DataPropertyName = "ProductOrderdeliverydate";
            this.ProductOrderdeliverydate.HeaderText = "হস্তাতরের তারিখ";
            this.ProductOrderdeliverydate.Name = "ProductOrderdeliverydate";
            // 
            // ProductName
            // 
            this.ProductName.DataPropertyName = "ProductName";
            this.ProductName.HeaderText = "পণ্যের নাম";
            this.ProductName.Name = "ProductName";
            // 
            // OrderQty
            // 
            this.OrderQty.DataPropertyName = "OrderQty";
            this.OrderQty.HeaderText = "পরিমান";
            this.OrderQty.Name = "OrderQty";
            // 
            // UnitName
            // 
            this.UnitName.DataPropertyName = "UnitName";
            this.UnitName.HeaderText = "পরিমাপের একক";
            this.UnitName.Name = "UnitName";
            // 
            // ProductOrderRemark
            // 
            this.ProductOrderRemark.DataPropertyName = "ProductOrderRemark";
            this.ProductOrderRemark.HeaderText = "মন্তব্য";
            this.ProductOrderRemark.Name = "ProductOrderRemark";
            // 
            // RequestForProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "RequestForProduct";
            this.Size = new System.Drawing.Size(1013, 685);
            this.Load += new System.EventHandler(this.RequestForProduct_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orderListDataGridView)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.productOrderGroupBox.ResumeLayout(false);
            this.productOrderGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox productOrderGroupBox;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView orderListDataGridView;
        private System.Windows.Forms.Label messagesLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductOrderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductOrderdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductOrderdeliverydate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductOrderRemark;
    }
}
