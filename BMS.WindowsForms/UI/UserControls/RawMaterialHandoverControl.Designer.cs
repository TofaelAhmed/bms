﻿namespace BMS.WindowsForms.UI.UserControls
{
    partial class RawMaterialHandoverControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rawMatarialNameComboBox = new System.Windows.Forms.ComboBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.rawMaterialHandoverDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.rawMaterialHandoverRemarkTextBox = new System.Windows.Forms.TextBox();
            this.Save = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.rawMaterialHandoverUnitTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rawMaterialHandoverdataGridView = new System.Windows.Forms.DataGridView();
            this.RawMaterialHandoverDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RawMaterialHandoverName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RawMaterialHandoverUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RawMaterialHandoverRemark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rawMaterialHandoverdataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(853, 488);
            this.panel1.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.rawMatarialNameComboBox);
            this.splitContainer1.Panel1.Controls.Add(this.deleteButton);
            this.splitContainer1.Panel1.Controls.Add(this.rawMaterialHandoverDateTimePicker);
            this.splitContainer1.Panel1.Controls.Add(this.rawMaterialHandoverRemarkTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.Save);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.rawMaterialHandoverUnitTextBox);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.rawMaterialHandoverdataGridView);
            this.splitContainer1.Size = new System.Drawing.Size(853, 488);
            this.splitContainer1.SplitterDistance = 184;
            this.splitContainer1.TabIndex = 0;
            // 
            // rawMatarialNameComboBox
            // 
            this.rawMatarialNameComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rawMatarialNameComboBox.FormattingEnabled = true;
            this.rawMatarialNameComboBox.Location = new System.Drawing.Point(8, 88);
            this.rawMatarialNameComboBox.Name = "rawMatarialNameComboBox";
            this.rawMatarialNameComboBox.Size = new System.Drawing.Size(167, 21);
            this.rawMatarialNameComboBox.TabIndex = 30;
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(27)))), ((int)(((byte)(76)))));
            this.deleteButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.deleteButton.FlatAppearance.BorderSize = 0;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deleteButton.Location = new System.Drawing.Point(45, 272);
            this.deleteButton.Margin = new System.Windows.Forms.Padding(0);
            this.deleteButton.MaximumSize = new System.Drawing.Size(68, 22);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Padding = new System.Windows.Forms.Padding(2);
            this.deleteButton.Size = new System.Drawing.Size(43, 19);
            this.deleteButton.TabIndex = 29;
            this.deleteButton.Text = "মুছুন";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // rawMaterialHandoverDateTimePicker
            // 
            this.rawMaterialHandoverDateTimePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rawMaterialHandoverDateTimePicker.Location = new System.Drawing.Point(8, 42);
            this.rawMaterialHandoverDateTimePicker.Name = "rawMaterialHandoverDateTimePicker";
            this.rawMaterialHandoverDateTimePicker.Size = new System.Drawing.Size(166, 20);
            this.rawMaterialHandoverDateTimePicker.TabIndex = 1;
            // 
            // rawMaterialHandoverRemarkTextBox
            // 
            this.rawMaterialHandoverRemarkTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rawMaterialHandoverRemarkTextBox.Location = new System.Drawing.Point(6, 188);
            this.rawMaterialHandoverRemarkTextBox.Multiline = true;
            this.rawMaterialHandoverRemarkTextBox.Name = "rawMaterialHandoverRemarkTextBox";
            this.rawMaterialHandoverRemarkTextBox.Size = new System.Drawing.Size(169, 64);
            this.rawMaterialHandoverRemarkTextBox.TabIndex = 4;
            // 
            // Save
            // 
            this.Save.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Save.Location = new System.Drawing.Point(102, 270);
            this.Save.MaximumSize = new System.Drawing.Size(73, 23);
            this.Save.MinimumSize = new System.Drawing.Size(73, 23);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(73, 23);
            this.Save.TabIndex = 5;
            this.Save.Text = "যুক্ত করুণ";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "মন্তব্য";
            // 
            // rawMaterialHandoverUnitTextBox
            // 
            this.rawMaterialHandoverUnitTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rawMaterialHandoverUnitTextBox.Location = new System.Drawing.Point(8, 138);
            this.rawMaterialHandoverUnitTextBox.Name = "rawMaterialHandoverUnitTextBox";
            this.rawMaterialHandoverUnitTextBox.Size = new System.Drawing.Size(166, 20);
            this.rawMaterialHandoverUnitTextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "পরিমান";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(5, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "কাঁচামালের নাম";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "তারিখ";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(184, 0);
            this.panel2.TabIndex = 0;
            // 
            // rawMaterialHandoverdataGridView
            // 
            this.rawMaterialHandoverdataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.rawMaterialHandoverdataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.rawMaterialHandoverdataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.rawMaterialHandoverdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rawMaterialHandoverdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RawMaterialHandoverDate,
            this.RawMaterialHandoverName,
            this.RawMaterialHandoverUnit,
            this.RawMaterialHandoverRemark});
            this.rawMaterialHandoverdataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rawMaterialHandoverdataGridView.Location = new System.Drawing.Point(0, 0);
            this.rawMaterialHandoverdataGridView.Name = "rawMaterialHandoverdataGridView";
            this.rawMaterialHandoverdataGridView.Size = new System.Drawing.Size(665, 488);
            this.rawMaterialHandoverdataGridView.TabIndex = 0;
            // 
            // RawMaterialHandoverDate
            // 
            this.RawMaterialHandoverDate.DataPropertyName = "RawMaterialHandoverDate";
            this.RawMaterialHandoverDate.HeaderText = "তারিখ";
            this.RawMaterialHandoverDate.Name = "RawMaterialHandoverDate";
            // 
            // RawMaterialHandoverName
            // 
            this.RawMaterialHandoverName.DataPropertyName = "RawMaterialHandoverName";
            this.RawMaterialHandoverName.HeaderText = "কাঁচামালের নাম";
            this.RawMaterialHandoverName.Name = "RawMaterialHandoverName";
            // 
            // RawMaterialHandoverUnit
            // 
            this.RawMaterialHandoverUnit.DataPropertyName = "RawMaterialHandoverUnit";
            this.RawMaterialHandoverUnit.HeaderText = "পরিমান";
            this.RawMaterialHandoverUnit.Name = "RawMaterialHandoverUnit";
            // 
            // RawMaterialHandoverRemark
            // 
            this.RawMaterialHandoverRemark.DataPropertyName = "RawMaterialHandoverRemark";
            this.RawMaterialHandoverRemark.HeaderText = "মন্তব্য";
            this.RawMaterialHandoverRemark.Name = "RawMaterialHandoverRemark";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(29, 19);
            this.label8.MaximumSize = new System.Drawing.Size(15, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 15);
            this.label8.TabIndex = 657;
            this.label8.Text = "*";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(63, 67);
            this.label5.MaximumSize = new System.Drawing.Size(15, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 15);
            this.label5.TabIndex = 658;
            this.label5.Text = "*";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(36, 115);
            this.label6.MaximumSize = new System.Drawing.Size(15, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 15);
            this.label6.TabIndex = 657;
            this.label6.Text = "*";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RawMaterialHandoverControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "RawMaterialHandoverControl";
            this.Size = new System.Drawing.Size(853, 488);
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rawMaterialHandoverdataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox rawMaterialHandoverUnitTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView rawMaterialHandoverdataGridView;
        private System.Windows.Forms.TextBox rawMaterialHandoverRemarkTextBox;
        private System.Windows.Forms.DateTimePicker rawMaterialHandoverDateTimePicker;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn RawMaterialHandoverDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RawMaterialHandoverName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RawMaterialHandoverUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn RawMaterialHandoverRemark;
        private System.Windows.Forms.ComboBox rawMatarialNameComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
    }
}
