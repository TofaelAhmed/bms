﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class StoredProductControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public StoredProductControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(display);
            BL.Start();
            GridviewDesign.Instance(storedProductInformationdataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            productComboboxDataLoad();
        }

        void display()
        {
            storedProductInformationdataGridView.AutoGenerateColumns = false;
            storedProductInformationdataGridView.DataSource = service.ProductionService.GetProductions();
        }
        public void productComboboxDataLoad()
        {
            var item = service.ProductDetailsService.GetProductDetails();
            var modified = item.Select(x => new { ProductName = x.ProductName, ProductId = x.ProductId }).ToList();
            productStoreComboBox.DataSource = new BindingSource(modified, null);
            productStoreComboBox.DisplayMember = "ProductName";
            productStoreComboBox.ValueMember = "ProductId";
            productStoreComboBox.SelectedIndex = -1;
        }
        private void Search_Click(object sender, EventArgs e)
        {

        }
    }
}
