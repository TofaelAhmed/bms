﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class EmployeeControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public EmployeeControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(display);
            BL.Start();
        }

        private void EmployeeInformation_Load(object sender, EventArgs e)
        {
            GridviewDesign.Instance(employeeInformationDataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            display();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            Employee obj = new Employee();
            obj.EmployeeName = employeeTextBox.Text.Trim();
            obj.EmployeeContactNo = mobileTextBox.Text.Trim();
            obj.EmployeeAddress = addressTextBox.Text.Trim();
            obj.EmployeeDesignation = designationTextBox.Text.Trim();

            service.EmployeeService.Save(obj);
            service.UnitOfWork.Commit();
            display();
        }
        void display()
        {
            employeeInformationDataGridView.AutoGenerateColumns = false;
            employeeInformationDataGridView.DataSource = service.EmployeeService.GetEmployees();
        }
    }
}
