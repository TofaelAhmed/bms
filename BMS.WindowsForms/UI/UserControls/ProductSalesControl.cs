﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;
using System.Threading;


namespace BMS.WindowsForms.UI.UserControls
{
    public partial class ProductSalesControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        List<VirtualModel3> items = new List<VirtualModel3>();
        decimal price = 10;

        public ProductSalesControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(productComboboxDataLoad);
            BL.Start();
            priceTextbox.Text = price.ToString();
            display();
            GridviewDesign.Instance(virtualDataGridView);
        }
  
        public void productComboboxDataLoad()
        {
            var item = service.ProductDetailsService.GetProductDetails();
            var modified = item.Select(x => new { ProductName = x.ProductName, ProductId = x.ProductId }).ToList();
            productNameComboBox.DataSource = new BindingSource(modified, null);
            productNameComboBox.DisplayMember = "ProductName";
            productNameComboBox.ValueMember = "ProductId";
            productNameComboBox.SelectedIndex = -1;
        }
        private void productNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            { 
                unitNameTextBox.Text = "";
                if (productNameComboBox.SelectedIndex < 0) return;
                var id = productNameComboBox.SelectedValue.ToString();
                decimal num;
                var isValidNum = decimal.TryParse(id, out num);
                if (isValidNum)
                {
                    var rawMaterialid = Convert.ToInt32(productNameComboBox.SelectedValue);
                    var item = service.RawMaterialService.GetRawMaterialById(rawMaterialid);
                    var unit = service.UnitService.GetUnitsById(item.UnitId);
                    unitNameTextBox.Text = unit.UnitName;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void display()
        {
            virtualDataGridView.AutoGenerateColumns = false;
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {

        }
        private void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                VirtualModel3 additems = new VirtualModel3();
                DateTime dtime;
                dtime = dateTimePicker.Value.Date;
                additems.Date = dtime;
                additems.Customar = customarTextBox.Text.ToString();
                additems.ContractNo = contractTextbox.Text.ToString();
                additems.Productname = productNameComboBox.Text.ToString();
                additems.Quantity = Convert.ToDecimal(quantityTextbox.Text.ToString());
                //additems.Price = Convert.ToDecimal(priceTextbox.Text.ToString());
                additems.Price = Convert.ToDecimal(priceTextbox.Text.ToString());
                additems.UnitName = unitNameTextBox.Text.ToString();
                additems.Commition = Convert.ToDecimal(commitionTextbox.Text.ToString());
                additems.Total = Convert.ToDecimal(totalTextBox.Text.ToString());

                items.Add(additems);
                virtualDataGridView.DataSource = null;
                virtualDataGridView.DataSource = items;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void quantityTextbox_TextChanged(object sender, EventArgs e)
        {
            if (quantityTextbox.Text.Trim() != "")
            {
                var qty = quantityTextbox.Text.Trim();
                decimal num;
                var isValidNum = decimal.TryParse(qty, out num);

                if (isValidNum)
                {
                    totalTextBox.Text = "";
                    decimal total = num * price;
                    totalTextBox.Text = total.ToString("N");
                }
            }
        }
       
    }
}