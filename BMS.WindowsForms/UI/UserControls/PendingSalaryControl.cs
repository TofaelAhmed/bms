﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BMS.Globals.Utilities;
using BMS.WindowsForms.Helper;
using BMS.Entities.BMS;

namespace BMS.WindowsForms.UI.UserControls
{
    public partial class PendingSalaryControl : UserControl
    {
        ServiceAccess service = new ServiceAccess();
        public PendingSalaryControl()
        {
            InitializeComponent();
            BackgroundLoader BL = new BackgroundLoader(display);
            BL.Start();
        }

        private void PendingSalaryInformation_Load(object sender, EventArgs e)
        {
            GridviewDesign.Instance(pendingSalaryInfoDataGridView);
            SplitContainerDesign.Instance(splitContainer1);
            display();
            comboboxdataload();
        }
        private void Save_Click(object sender, EventArgs e)
        {
          

            service.UnitOfWork.Commit();
            display();
        }
        void display()
        {
            pendingSalaryInfoDataGridView.AutoGenerateColumns = false;
        }
        void comboboxdataload()
        {
            employeeNameComboBox.DataSource = new BindingSource(service.EmployeeService.GetEmployees(), null);
            employeeNameComboBox.DisplayMember = "EmployeeName";
            employeeNameComboBox.ValueMember = "EmployeeId";
            employeeNameComboBox.SelectedIndex = -1;
        }
    }
}
