﻿namespace BMS.WindowsForms.UI
{
    partial class MainUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUi));
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.RawMaterialStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DetailedInformationOnRawMaterialsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ByeRawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RequestForRawMaterialsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeliveryRawMaterialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RawMaterialStockeinfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductDetailInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductionInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RequestForProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StokedProductInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RequestForRawMetarialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ByerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SalesReturnProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CustomerDuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DuseClearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DuesClearHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.পণযঅদরগরহণToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DesignationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EmployeeInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PaidSalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SalaryInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SalaryDuesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeControlLabel = new System.Windows.Forms.Label();
            this.mainBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.controlPanel = new System.Windows.Forms.Panel();
            this.mainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.BackColor = System.Drawing.SystemColors.MenuBar;
            this.mainMenuStrip.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainMenuStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RawMaterialStripMenuItem,
            this.ProductionToolStripMenuItem,
            this.SalesToolStripMenuItem,
            this.SalaryToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Padding = new System.Windows.Forms.Padding(8);
            this.mainMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.mainMenuStrip.Size = new System.Drawing.Size(1008, 44);
            this.mainMenuStrip.TabIndex = 1;
            this.mainMenuStrip.Text = "menuStrip";
            // 
            // RawMaterialStripMenuItem
            // 
            this.RawMaterialStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DetailedInformationOnRawMaterialsToolStripMenuItem,
            this.ByeRawToolStripMenuItem,
            this.RequestForRawMaterialsToolStripMenuItem,
            this.DeliveryRawMaterialToolStripMenuItem,
            this.RawMaterialStockeinfoToolStripMenuItem});
            this.RawMaterialStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RawMaterialStripMenuItem.Image = global::BMS.WindowsForms.Properties.Resources.folder;
            this.RawMaterialStripMenuItem.Name = "RawMaterialStripMenuItem";
            this.RawMaterialStripMenuItem.Size = new System.Drawing.Size(179, 28);
            this.RawMaterialStripMenuItem.Text = "কাঁচামাল সংক্রান্ত তথ্য";
            // 
            // DetailedInformationOnRawMaterialsToolStripMenuItem
            // 
            this.DetailedInformationOnRawMaterialsToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailedInformationOnRawMaterialsToolStripMenuItem.Name = "DetailedInformationOnRawMaterialsToolStripMenuItem";
            this.DetailedInformationOnRawMaterialsToolStripMenuItem.Size = new System.Drawing.Size(264, 28);
            this.DetailedInformationOnRawMaterialsToolStripMenuItem.Text = "কাঁচামালের বিস্তারিত তথ্য";
            this.DetailedInformationOnRawMaterialsToolStripMenuItem.Click += new System.EventHandler(this.DetailedInformationOnRawMaterialsToolStripMenuItem_Click);
            // 
            // ByeRawToolStripMenuItem
            // 
            this.ByeRawToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ByeRawToolStripMenuItem.Name = "ByeRawToolStripMenuItem";
            this.ByeRawToolStripMenuItem.Size = new System.Drawing.Size(264, 28);
            this.ByeRawToolStripMenuItem.Text = "কাঁচামাল ক্রয়ের তথ্য";
            this.ByeRawToolStripMenuItem.Click += new System.EventHandler(this.ByeRawToolStripMenuItem_Click);
            // 
            // RequestForRawMaterialsToolStripMenuItem
            // 
            this.RequestForRawMaterialsToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RequestForRawMaterialsToolStripMenuItem.Name = "RequestForRawMaterialsToolStripMenuItem";
            this.RequestForRawMaterialsToolStripMenuItem.Size = new System.Drawing.Size(264, 28);
            this.RequestForRawMaterialsToolStripMenuItem.Text = "অনুরোধকৃত কাঁচামালের তথ্য";
            this.RequestForRawMaterialsToolStripMenuItem.Click += new System.EventHandler(this.RequestedRawMaterialsToolStripMenuItem_Click);
            // 
            // DeliveryRawMaterialToolStripMenuItem
            // 
            this.DeliveryRawMaterialToolStripMenuItem.Name = "DeliveryRawMaterialToolStripMenuItem";
            this.DeliveryRawMaterialToolStripMenuItem.Size = new System.Drawing.Size(264, 28);
            this.DeliveryRawMaterialToolStripMenuItem.Text = "কাঁচামাল হস্তান্তেরের তথ্য";
            this.DeliveryRawMaterialToolStripMenuItem.Click += new System.EventHandler(this.DeliveryRawMaterialToolStripMenuItem_Click);
            // 
            // RawMaterialStockeinfoToolStripMenuItem
            // 
            this.RawMaterialStockeinfoToolStripMenuItem.Name = "RawMaterialStockeinfoToolStripMenuItem";
            this.RawMaterialStockeinfoToolStripMenuItem.Size = new System.Drawing.Size(264, 28);
            this.RawMaterialStockeinfoToolStripMenuItem.Text = "মজুদকৃত কাঁচামালের তথ্য";
            this.RawMaterialStockeinfoToolStripMenuItem.Click += new System.EventHandler(this.RawMaterialStockeinfoToolStripMenuItem_Click);
            // 
            // ProductionToolStripMenuItem
            // 
            this.ProductionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProductDetailInfoToolStripMenuItem,
            this.ProductionInfoToolStripMenuItem,
            this.RequestForProductToolStripMenuItem,
            this.StokedProductInfoToolStripMenuItem,
            this.RequestForRawMetarialToolStripMenuItem});
            this.ProductionToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductionToolStripMenuItem.Image = global::BMS.WindowsForms.Properties.Resources.factory;
            this.ProductionToolStripMenuItem.Name = "ProductionToolStripMenuItem";
            this.ProductionToolStripMenuItem.Size = new System.Drawing.Size(211, 28);
            this.ProductionToolStripMenuItem.Text = "পণ্য উৎপাদন সংক্রান্ত তথ্য";
            // 
            // ProductDetailInfoToolStripMenuItem
            // 
            this.ProductDetailInfoToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductDetailInfoToolStripMenuItem.Name = "ProductDetailInfoToolStripMenuItem";
            this.ProductDetailInfoToolStripMenuItem.Size = new System.Drawing.Size(291, 28);
            this.ProductDetailInfoToolStripMenuItem.Text = "পণ্যের বিস্তারিত তথ্য";
            this.ProductDetailInfoToolStripMenuItem.Click += new System.EventHandler(this.ProductToolStripMenuItem_Click);
            // 
            // ProductionInfoToolStripMenuItem
            // 
            this.ProductionInfoToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductionInfoToolStripMenuItem.Name = "ProductionInfoToolStripMenuItem";
            this.ProductionInfoToolStripMenuItem.Size = new System.Drawing.Size(291, 28);
            this.ProductionInfoToolStripMenuItem.Text = "উৎপাদিত পণ্যের তথ্য";
            this.ProductionInfoToolStripMenuItem.Click += new System.EventHandler(this.ProductionInfoToolStripMenuItem_Click);
            // 
            // RequestForProductToolStripMenuItem
            // 
            this.RequestForProductToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RequestForProductToolStripMenuItem.Name = "RequestForProductToolStripMenuItem";
            this.RequestForProductToolStripMenuItem.Size = new System.Drawing.Size(291, 28);
            this.RequestForProductToolStripMenuItem.Text = "অনুরোধকৃত পণ্যের তথ্য";
            this.RequestForProductToolStripMenuItem.Click += new System.EventHandler(this.RequestForProductToolStripMenuItem_Click);
            // 
            // StokedProductInfoToolStripMenuItem
            // 
            this.StokedProductInfoToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StokedProductInfoToolStripMenuItem.Name = "StokedProductInfoToolStripMenuItem";
            this.StokedProductInfoToolStripMenuItem.Size = new System.Drawing.Size(291, 28);
            this.StokedProductInfoToolStripMenuItem.Text = "মজুদকৃত পণ্যের তথ্য";
            this.StokedProductInfoToolStripMenuItem.Click += new System.EventHandler(this.StokedProductInfoToolStripMenuItem_Click);
            // 
            // RequestForRawMetarialToolStripMenuItem
            // 
            this.RequestForRawMetarialToolStripMenuItem.Name = "RequestForRawMetarialToolStripMenuItem";
            this.RequestForRawMetarialToolStripMenuItem.Size = new System.Drawing.Size(291, 28);
            this.RequestForRawMetarialToolStripMenuItem.Text = "কাঁচামালের জন্য অনুরোধের  তথ্য";
            this.RequestForRawMetarialToolStripMenuItem.Click += new System.EventHandler(this.RequestForRawMetarialToolStripMenuItem_Click);
            // 
            // SalesToolStripMenuItem
            // 
            this.SalesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ByerToolStripMenuItem,
            this.SalesProductToolStripMenuItem,
            this.SalesReturnProductToolStripMenuItem,
            this.CustomerDuesToolStripMenuItem,
            this.DuseClearToolStripMenuItem,
            this.DuesClearHistoryToolStripMenuItem,
            this.পণযঅদরগরহণToolStripMenuItem});
            this.SalesToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalesToolStripMenuItem.Image = global::BMS.WindowsForms.Properties.Resources.shopping;
            this.SalesToolStripMenuItem.Name = "SalesToolStripMenuItem";
            this.SalesToolStripMenuItem.Size = new System.Drawing.Size(172, 28);
            this.SalesToolStripMenuItem.Text = "বিপণন সংক্রান্ত তথ্য ";
            // 
            // ByerToolStripMenuItem
            // 
            this.ByerToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ByerToolStripMenuItem.Name = "ByerToolStripMenuItem";
            this.ByerToolStripMenuItem.Size = new System.Drawing.Size(278, 28);
            this.ByerToolStripMenuItem.Text = "ক্রেতার তথ্য";
            this.ByerToolStripMenuItem.Click += new System.EventHandler(this.ByerToolStripMenuItem_Click);
            // 
            // SalesProductToolStripMenuItem
            // 
            this.SalesProductToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalesProductToolStripMenuItem.Name = "SalesProductToolStripMenuItem";
            this.SalesProductToolStripMenuItem.Size = new System.Drawing.Size(278, 28);
            this.SalesProductToolStripMenuItem.Text = "পণ্য বিক্রয়ের তথ্য";
            this.SalesProductToolStripMenuItem.Click += new System.EventHandler(this.SalesProductToolStripMenuItem_Click);
            // 
            // SalesReturnProductToolStripMenuItem
            // 
            this.SalesReturnProductToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalesReturnProductToolStripMenuItem.Name = "SalesReturnProductToolStripMenuItem";
            this.SalesReturnProductToolStripMenuItem.Size = new System.Drawing.Size(278, 28);
            this.SalesReturnProductToolStripMenuItem.Text = "বিক্রয় ফেরত তথ্য";
            // 
            // CustomerDuesToolStripMenuItem
            // 
            this.CustomerDuesToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustomerDuesToolStripMenuItem.Name = "CustomerDuesToolStripMenuItem";
            this.CustomerDuesToolStripMenuItem.Size = new System.Drawing.Size(278, 28);
            this.CustomerDuesToolStripMenuItem.Text = "ক্রেতার বাকীর পরিমাণ";
            this.CustomerDuesToolStripMenuItem.Click += new System.EventHandler(this.CustomerDuesToolStripMenuItem_Click);
            // 
            // DuseClearToolStripMenuItem
            // 
            this.DuseClearToolStripMenuItem.Name = "DuseClearToolStripMenuItem";
            this.DuseClearToolStripMenuItem.Size = new System.Drawing.Size(278, 28);
            this.DuseClearToolStripMenuItem.Text = "ক্রেতার বকেয়া পরিশোধের তথ্য";
            // 
            // DuesClearHistoryToolStripMenuItem
            // 
            this.DuesClearHistoryToolStripMenuItem.Name = "DuesClearHistoryToolStripMenuItem";
            this.DuesClearHistoryToolStripMenuItem.Size = new System.Drawing.Size(278, 28);
            this.DuesClearHistoryToolStripMenuItem.Text = "মুল্য পরিশোধের তথ্য";
            this.DuesClearHistoryToolStripMenuItem.Click += new System.EventHandler(this.DuesClearHistoryToolStripMenuItem_Click);
            // 
            // পণযঅদরগরহণToolStripMenuItem
            // 
            this.পণযঅদরগরহণToolStripMenuItem.Name = "পণযঅদরগরহণToolStripMenuItem";
            this.পণযঅদরগরহণToolStripMenuItem.Size = new System.Drawing.Size(278, 28);
            this.পণযঅদরগরহণToolStripMenuItem.Text = "পণ্যের জন্য অনুরোধ";
            this.পণযঅদরগরহণToolStripMenuItem.Click += new System.EventHandler(this.ProductOrderControlToolStripMenuItem_Click);
            // 
            // SalaryToolStripMenuItem
            // 
            this.SalaryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DesignationToolStripMenuItem,
            this.EmployeeInfoToolStripMenuItem,
            this.PaidSalaryToolStripMenuItem,
            this.SalaryInfoToolStripMenuItem,
            this.SalaryDuesToolStripMenuItem});
            this.SalaryToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalaryToolStripMenuItem.Image = global::BMS.WindowsForms.Properties.Resources._625599;
            this.SalaryToolStripMenuItem.Name = "SalaryToolStripMenuItem";
            this.SalaryToolStripMenuItem.Size = new System.Drawing.Size(206, 28);
            this.SalaryToolStripMenuItem.Text = "বেতন বকেয়া সংক্রান্ত তথ্য";
            // 
            // DesignationToolStripMenuItem
            // 
            this.DesignationToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DesignationToolStripMenuItem.Name = "DesignationToolStripMenuItem";
            this.DesignationToolStripMenuItem.Size = new System.Drawing.Size(222, 28);
            this.DesignationToolStripMenuItem.Text = "পদবী";
            this.DesignationToolStripMenuItem.Click += new System.EventHandler(this.DesignationToolStripMenuItem_Click);
            // 
            // EmployeeInfoToolStripMenuItem
            // 
            this.EmployeeInfoToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmployeeInfoToolStripMenuItem.Name = "EmployeeInfoToolStripMenuItem";
            this.EmployeeInfoToolStripMenuItem.Size = new System.Drawing.Size(222, 28);
            this.EmployeeInfoToolStripMenuItem.Text = "কর্মচারির তথ্য";
            this.EmployeeInfoToolStripMenuItem.Click += new System.EventHandler(this.EmployeeInfoToolStripMenuItem_Click);
            // 
            // PaidSalaryToolStripMenuItem
            // 
            this.PaidSalaryToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PaidSalaryToolStripMenuItem.Name = "PaidSalaryToolStripMenuItem";
            this.PaidSalaryToolStripMenuItem.Size = new System.Drawing.Size(222, 28);
            this.PaidSalaryToolStripMenuItem.Text = "বেতন পরিশোধের তথ্য";
            this.PaidSalaryToolStripMenuItem.Click += new System.EventHandler(this.PaidSalaryToolStripMenuItem_Click);
            // 
            // SalaryInfoToolStripMenuItem
            // 
            this.SalaryInfoToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalaryInfoToolStripMenuItem.Name = "SalaryInfoToolStripMenuItem";
            this.SalaryInfoToolStripMenuItem.Size = new System.Drawing.Size(222, 28);
            this.SalaryInfoToolStripMenuItem.Text = "বেতনরে তথ্য";
            this.SalaryInfoToolStripMenuItem.Click += new System.EventHandler(this.SalaryInfoToolStripMenuItem_Click);
            // 
            // SalaryDuesToolStripMenuItem
            // 
            this.SalaryDuesToolStripMenuItem.Name = "SalaryDuesToolStripMenuItem";
            this.SalaryDuesToolStripMenuItem.Size = new System.Drawing.Size(222, 28);
            this.SalaryDuesToolStripMenuItem.Text = "বকেয়া বেতন";
            this.SalaryDuesToolStripMenuItem.Click += new System.EventHandler(this.SalaryDuesToolStripMenuItem_Click);
            // 
            // closeControlLabel
            // 
            this.closeControlLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closeControlLabel.AutoSize = true;
            this.closeControlLabel.BackColor = System.Drawing.SystemColors.MenuBar;
            this.closeControlLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeControlLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeControlLabel.ForeColor = System.Drawing.Color.Maroon;
            this.closeControlLabel.Location = new System.Drawing.Point(981, 25);
            this.closeControlLabel.Margin = new System.Windows.Forms.Padding(8);
            this.closeControlLabel.Name = "closeControlLabel";
            this.closeControlLabel.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.closeControlLabel.Size = new System.Drawing.Size(27, 17);
            this.closeControlLabel.TabIndex = 14;
            this.closeControlLabel.Text = "X";
            this.closeControlLabel.Visible = false;
            this.closeControlLabel.Click += new System.EventHandler(this.closeControlLabel_Click);
            // 
            // controlPanel
            // 
            this.controlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controlPanel.AutoSize = true;
            this.controlPanel.BackColor = System.Drawing.SystemColors.MenuBar;
            this.controlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.controlPanel.Location = new System.Drawing.Point(0, 44);
            this.controlPanel.Margin = new System.Windows.Forms.Padding(4);
            this.controlPanel.Name = "controlPanel";
            this.controlPanel.Padding = new System.Windows.Forms.Padding(8);
            this.controlPanel.Size = new System.Drawing.Size(1008, 685);
            this.controlPanel.TabIndex = 2;
            // 
            // MainUi
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.closeControlLabel);
            this.Controls.Add(this.controlPanel);
            this.Controls.Add(this.mainMenuStrip);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainUi";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem RawMaterialStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DetailedInformationOnRawMaterialsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ByeRawToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RequestForRawMaterialsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProductDetailInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProductionInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RequestForProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StokedProductInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ByerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SalesProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SalesReturnProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CustomerDuesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DuseClearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DesignationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EmployeeInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PaidSalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SalaryInfoToolStripMenuItem;
        private System.Windows.Forms.Label closeControlLabel;
        private System.Windows.Forms.ToolStripMenuItem DeliveryRawMaterialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RawMaterialStockeinfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RequestForRawMetarialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DuesClearHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SalaryDuesToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker mainBackgroundWorker;
        private System.Windows.Forms.Panel controlPanel;
        private System.Windows.Forms.ToolStripMenuItem পণযঅদরগরহণToolStripMenuItem;
    }
}