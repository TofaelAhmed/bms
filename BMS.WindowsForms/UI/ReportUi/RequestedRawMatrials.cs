﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMS.WindowsForms.UI.ReportUi
{
    public partial class RequestedRawMatrials : Form
    {
        private readonly DataTable _requestedRawMatrial;
        public RequestedRawMatrials(DataTable requestedRawMatrial)
        {
            InitializeComponent();
            _requestedRawMatrial = requestedRawMatrial;
            
        }

        private void RequestedRawMatrials_Load(object sender, EventArgs e)
        {
            dataSetBindingSource.DataSource = _requestedRawMatrial;
            this.reportViewer1.RefreshReport();
        }
    }
}
