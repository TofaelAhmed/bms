﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IStoredRawMaterialServise
    {
        void Save(StoredRawMaterial obj);
        void Edit(StoredRawMaterial obj);
        IEnumerable<StoredRawMaterial> GetStoredRawMaterials();
    }
    public class StoredRawMaterialServise : IStoredRawMaterialServise
    {
        private IStoredRawMaterialRepository _storedRawMaterialRepository;
        private IUnitOfWork _unitOfWork;
        public StoredRawMaterialServise(IStoredRawMaterialRepository storedRawMaterialRepository, IUnitOfWork unitOfWork)
        {
            _storedRawMaterialRepository = storedRawMaterialRepository;
            _unitOfWork = unitOfWork;
        }
        public void Edit(StoredRawMaterial obj)
        {
            _storedRawMaterialRepository.Update(obj);
        }

        public IEnumerable<StoredRawMaterial> GetStoredRawMaterials()
        {
            return _storedRawMaterialRepository.GetAll();
        }

        public void Save(StoredRawMaterial obj)
        {
            _storedRawMaterialRepository.Add(obj);
        }
    }
}
