﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IRawMaterialBuyService
    {
        void Save(RawMaterialBuy obj);
        void Edit(RawMaterialBuy obj);
        IEnumerable<RawMaterialBuy> GetRawMaterialBuys();
    }
    public class RawMaterialBuyService : IRawMaterialBuyService
    {
        private IRawMaterialBuyRepository _rawMaterialBuyRepository;
        private IUnitOfWork _unitofWork;
        public RawMaterialBuyService(IRawMaterialBuyRepository rawMaterialBuyRepository, IUnitOfWork unitOfWork)
        {
            _rawMaterialBuyRepository = rawMaterialBuyRepository;
            _unitofWork = unitOfWork;
        }
        public void Edit(RawMaterialBuy obj)
        {
            _rawMaterialBuyRepository.Update(obj);
        }

        public IEnumerable<RawMaterialBuy> GetRawMaterialBuys()
        {
            return _rawMaterialBuyRepository.GetAll();
        }

        public void Save(RawMaterialBuy obj)
        {
            _rawMaterialBuyRepository.Add(obj);
        }
    }
}
