﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IRequestedProductService
    {
        void Save(RequestedProduct obj);
        void Edit(RequestedProduct obj);
        IEnumerable<RequestedProduct> GetRequestedProducts();
    }
    public class RequestedProductService: IRequestedProductService
    {
        private IRequestedProductRepository _requestedProductRepository;
        private IUnitOfWork _unitOfWork;
        public RequestedProductService(IRequestedProductRepository requestedProductRepository, IUnitOfWork unitOfWork)
        {
            _requestedProductRepository = requestedProductRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(RequestedProduct obj)
        {
            _requestedProductRepository.Update(obj);
        }

        public IEnumerable<RequestedProduct> GetRequestedProducts()
        {
            return _requestedProductRepository.GetAll();
        }

        public void Save(RequestedProduct obj)
        {
            _requestedProductRepository.Add(obj);
        }
    }
}
