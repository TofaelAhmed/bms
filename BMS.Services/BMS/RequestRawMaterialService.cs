﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IRequestRawMaterialService
    {
        void Save(RequestRawMaterial obj);
        void Edit(RequestRawMaterial obj);
        IEnumerable<RequestRawMaterial> GetRequestRawMaterials();
        DataTable GetRequestedRawMaterials();
    }
    public class RequestRawMaterialService: IRequestRawMaterialService
    {
        private IRequestRawMaterialRepository _requestRawMaterialRepository;
        private IUnitOfWork _unitOfWork;
        public RequestRawMaterialService(IRequestRawMaterialRepository requestRawMaterialRepository, IUnitOfWork unitOfWork)
        {
            _requestRawMaterialRepository = requestRawMaterialRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(RequestRawMaterial obj)
        {
            _requestRawMaterialRepository.Update(obj);
        }

        public IEnumerable<RequestRawMaterial> GetRequestRawMaterials()
        {
            return _requestRawMaterialRepository.GetAll();
        }

        public void Save(RequestRawMaterial obj)
        {
            _requestRawMaterialRepository.Add(obj);
        }
        public DataTable GetRequestedRawMaterials()
        {
            return _requestRawMaterialRepository.GetRequestedRawMaterials();
        }
    }
}
