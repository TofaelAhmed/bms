﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface ISalaryPaymentServise
    {
        void Save(SalaryPayment obj);
        void Edit(SalaryPayment obj);
        IEnumerable<SalaryPayment> GetSalaryPayments();
    }
    public class SalaryPaymentServise : ISalaryPaymentServise
    {
        private ISalaryPaymentRepository _salaryPaymentRepository;
        private IUnitOfWork _unitOfWork;
        public SalaryPaymentServise(ISalaryPaymentRepository salaryPaymentRepository, IUnitOfWork unitOfWork)
        {
            _salaryPaymentRepository = salaryPaymentRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(SalaryPayment obj)
        {
            _salaryPaymentRepository.Update(obj);
        }

        public IEnumerable<SalaryPayment> GetSalaryPayments()
        {
            return _salaryPaymentRepository.GetAll();
        }

        public void Save(SalaryPayment obj)
        {
            _salaryPaymentRepository.Add(obj);
        }
    }
}
