﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IRawMaterialService
    {
        void Save(RawMaterial obj);
        void Edit(RawMaterial obj);
        IEnumerable<RawMaterial> GetRawMaterials();
        RawMaterial GetRawMaterialById(int id);
    }
    public class RawMaterialService: IRawMaterialService
    {
        private IRawMaterialRepository _rawMaterialRepository;
        private IUnitOfWork _unitOfWork;
        public RawMaterialService(IRawMaterialRepository rawMaterialRepository, IUnitOfWork unitOfWork)
        {
            _rawMaterialRepository = rawMaterialRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(RawMaterial obj)
        {
            _rawMaterialRepository.Update(obj);
        }

        public IEnumerable<RawMaterial> GetRawMaterials()
        {
            return _rawMaterialRepository.GetRawMaterials();
        }

        public void Save(RawMaterial obj)
        {
            _rawMaterialRepository.Add(obj);
        }
        public RawMaterial GetRawMaterialById(int id)
        {
           return _rawMaterialRepository.GetRawMaterialById(id);
        }
    }
}
