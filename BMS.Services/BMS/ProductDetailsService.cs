﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IProductDetailsService
    {
        void Save(ProductDetails obj);
        void Edit(ProductDetails obj);
        IEnumerable<ProductDetails> GetProductDetails();
        ProductDetails GetProductDetailById(int id);
    }
    public class ProductDetailsService : IProductDetailsService
    {
        private IProductDetailsRepository _productDetailsRepository;
        private IUnitOfWork _unitOfWork;
        public ProductDetailsService(IProductDetailsRepository productDetailsRepository, IUnitOfWork unitOfWork)
        {
            _productDetailsRepository = productDetailsRepository;
            _unitOfWork = unitOfWork;
        }
        public void Edit(ProductDetails obj)
        {
            _productDetailsRepository.Update(obj);
        }

        public ProductDetails GetProductDetailById(int id)
        {
            return _productDetailsRepository.GetProductDetailsById(id);
        }

        public IEnumerable<ProductDetails> GetProductDetails()
        {
            return _productDetailsRepository.GetAll();
        }

        public void Save(ProductDetails obj)
        {
            _productDetailsRepository.Save(obj);
        }
    }
}
