﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IProductOrderedService
    {
        void Save(ProductOrdered obj);
        IEnumerable<ProductOrdered> GetProductOrdereds();
        List<ProductOrdered> GetByProductId(int id);
        ProductOrdered GetById(int id);
        List<ProductOrdered> PendingOrdered();
        List<ProductOrdered> PendingOrderedRawMaterial();
        void Edit(ProductOrdered item);
    }
    public class ProductOrderedService : IProductOrderedService
    {
        private IProductOrderedRepository _productOrderedRepository;
        private IUnitOfWork _unitOfWork;
        public ProductOrderedService(IProductOrderedRepository productOrderedRepository, IUnitOfWork unitOfWork)
        {
            _productOrderedRepository = productOrderedRepository;
            _unitOfWork = unitOfWork;
        }
        public IEnumerable<ProductOrdered> GetProductOrdereds()
        {
            return _productOrderedRepository.GetAll();
        }
        public ProductOrdered GetById(int id)
        {
            return _productOrderedRepository.GetById(id);
        }
        public List<ProductOrdered> GetByProductId(int id)
        {
            return _productOrderedRepository.GetByProductId(id);
        }

        public void Save(ProductOrdered obj)
        {
            _productOrderedRepository.Add(obj);
        }
        public void Edit(ProductOrdered item)
        {
            _productOrderedRepository.Edit(item);
        }
        public List<ProductOrdered> PendingOrdered()
        {
            return _productOrderedRepository.PendingOrdered();
        }
        public List<ProductOrdered> PendingOrderedRawMaterial()
        {
            return _productOrderedRepository.PendingOrderedRawMaterial();
        }
    }
}
