﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IStoredProductServise
    {
        void Save(StoredProduct obj);
        void Edit(StoredProduct obj);
        IEnumerable<StoredProduct> GetStoredProducts();
    }
    public class StoredProductServise: IStoredProductServise
    {
        private IStoredProductRepository _storedProductRepository;
        private IUnitOfWork _unitOfWork;
        public StoredProductServise(IStoredProductRepository storedProductRepository, IUnitOfWork unitOfWork)
        {
            _storedProductRepository = storedProductRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(StoredProduct obj)
        {
            _storedProductRepository.Update(obj);
        }

        public IEnumerable<StoredProduct> GetStoredProducts()
        {
            return _storedProductRepository.GetAll();
        }

        public void Save(StoredProduct obj)
        {
            _storedProductRepository.Add(obj);
        }
    }
}
