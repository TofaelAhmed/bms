﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface ISalaryService
    {
        void Save(Salary obj);
        void Edit(Salary obj);
        IEnumerable<Salary> GetSalaryServices();
    }
    public class SalaryService: ISalaryService
    {
        private ISalaryRepository _salaryRepository;
        private IUnitOfWork _unitOfWork;
        public SalaryService(ISalaryRepository salaryRepository, IUnitOfWork unitOfWork)
        {
            _salaryRepository = salaryRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(Salary obj)
        {
            _salaryRepository.Update(obj);
        }

        public IEnumerable<Salary> GetSalaryServices()
        {
            return _salaryRepository.GetAll();
        }

        public void Save(Salary obj)
        {
            _salaryRepository.Add(obj);
        }
    }
}
