﻿using BMS.Data.Repository.BMS;
using BMS.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMS.Entities.BMS;

namespace BMS.Services.BMS
{
    public interface IProductRawmaterialMappingService
    {
        void Save(ProductRawmaterialMapping obj);
        IEnumerable<ProductRawmaterialMapping> GetProductRawmaterialMappings();
        List<ProductRawmaterialMapping> GetProductRawmaterialMappingsById(int id);
    }
    public class ProductRawmaterialMappingService: IProductRawmaterialMappingService
    {
        private IProductRawmaterialMappingRepository _productRawmaterialMappingRepository;
        private IUnitOfWork _unitOfWork;
        public ProductRawmaterialMappingService(IProductRawmaterialMappingRepository productRawmaterialMappingRepository, IUnitOfWork unitOfwork)
        {
            _productRawmaterialMappingRepository = productRawmaterialMappingRepository;
            _unitOfWork = unitOfwork;
        }

        public IEnumerable<ProductRawmaterialMapping> GetProductRawmaterialMappings()
        {
            return _productRawmaterialMappingRepository.GetAll();
        }

        public void Save(ProductRawmaterialMapping obj)
        {
            _productRawmaterialMappingRepository.Add(obj);
        }

        public List<ProductRawmaterialMapping> GetProductRawmaterialMappingsById(int id)
        {
            return _productRawmaterialMappingRepository.GetProductRawmaterialMappingById(id);
        }
    }
}
