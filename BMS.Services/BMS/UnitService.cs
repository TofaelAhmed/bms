﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IUnitService
    {
        IEnumerable<Unit> GetUnits();
        Unit GetUnitsById(int id);
    }
    public class UnitService: IUnitService
    {
        private IUnitRepository _unitRepository;
        private IUnitOfWork _unitOfWork;
        public UnitService(IUnitRepository unitRepository, IUnitOfWork unitOfWork)
        {
            _unitRepository = unitRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Unit> GetUnits()
        {
            return _unitRepository.GetAll();
        }
        public Unit GetUnitsById(int id)
        {
            return _unitRepository.GetUnitsById(id);
        }
    }
}
