﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IEmployeeService
    {
        void Save(Employee obj);
        void Edit(Employee obj);
        IEnumerable<Employee> GetEmployees();
    }
    public class EmployeeService:IEmployeeService
    {
        private IEmployeeRepository _employeeRepository;
        private IUnitOfWork _unitOfWork;
        public EmployeeService(IEmployeeRepository employeeRepository, IUnitOfWork unitOfWork)
        {
            _employeeRepository = employeeRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(Employee obj)
        {
            _employeeRepository.Update(obj);
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return _employeeRepository.GetAll();
        }

        public void Save(Employee obj)
        {
            _employeeRepository.Add(obj);
        }
    }
}