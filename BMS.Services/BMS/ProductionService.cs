﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IProductionService
    {
        void Save(Production obj);
        void Edit(Production obj);
        IEnumerable<Production> GetProductions();
    }
    public class ProductionService: IProductionService
    {
        private IProductionRepository _productionRepository;
        private IUnitOfWork _unitOfWork;
        public ProductionService(IProductionRepository productionRepository, IUnitOfWork unitOfWork)
        {
            _productionRepository = productionRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(Production obj)
        {
            _productionRepository.Update(obj);
        }

        public IEnumerable<Production> GetProductions()
        {
            return _productionRepository.GetAll();
        }

        public void Save(Production obj)
        {
            _productionRepository.Add(obj);
        }
    }
}
