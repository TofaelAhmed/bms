﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IRawMaterialHandoverService
    {
        void Save(RawMaterialHandover obj);
        void Edit(RawMaterialHandover obj);
        IEnumerable<RawMaterialHandover> GetRawMaterialHandovers();
    }
    public class RawMaterialHandoverService: IRawMaterialHandoverService
    {
        private IRawMaterialHandoverRepository _rawMaterialHandoverRepository;
        private IUnitOfWork _unitOfWork;
        public RawMaterialHandoverService(IRawMaterialHandoverRepository rawMaterialHandoverRepository, IUnitOfWork unitOfWork)
        {
            _rawMaterialHandoverRepository = rawMaterialHandoverRepository;
            _unitOfWork = unitOfWork;
        }

        public RawMaterialHandoverService(RawMaterialHandoverRepository rawMaterialHandoverRepository, IUnitOfWork unitOfWork)
        {
            _rawMaterialHandoverRepository = rawMaterialHandoverRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(RawMaterialHandover obj)
        {
            _rawMaterialHandoverRepository.Update(obj);
        }

        public IEnumerable<RawMaterialHandover> GetRawMaterialHandovers()
        {
            return _rawMaterialHandoverRepository.GetAll();
        }

        public void Save(RawMaterialHandover obj)
        {
            _rawMaterialHandoverRepository.Add(obj);
        }
    }
}
