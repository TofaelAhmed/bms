﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IProductOrderService
    {
        void Save(ProductOrdered obj);
        IEnumerable<ProductOrdered> GetProductOrders();
        List<ProductOrdered> GetProductOrderById(int id);
    }
    public class ProductOrderService : IProductOrderService
    {
        private IProductOrderedRepository _productOrderedRepository;
        private IUnitOfWork _unitOfWork;
        public ProductOrderService(IProductOrderedRepository productOrderedRepository, IUnitOfWork unitOfWork)
        {
            _productOrderedRepository = productOrderedRepository;
            _unitOfWork = unitOfWork;
        }
        public List<ProductOrdered> GetProductOrderById(int id)
        {
            return _productOrderedRepository.GetProductOrderedsById(id);
        }

        public IEnumerable<ProductOrdered> GetProductOrders()
        {
            return _productOrderedRepository.GetAll();
        }

        public void Save(ProductOrdered obj)
        {
            _productOrderedRepository.Add(obj);
        }
    }
}
