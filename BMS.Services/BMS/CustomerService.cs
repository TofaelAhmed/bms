﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface ICustomerService
    {
        void Save(Customar obj);
        void Edit(Customar obj);
        IEnumerable<Customar> GetCustomars();
    }
    public class CustomerService : ICustomerService
    {
        private ICustomerRepository _customerRepository;
        private IUnitOfWork _unitOfWork;
        public CustomerService(ICustomerRepository customerRepository, IUnitOfWork unitOfWork)
        {
            _customerRepository = customerRepository;
            _unitOfWork = unitOfWork;
        }
        public void Edit(Customar obj)
        {
            _customerRepository.Edit(obj);
        }

        public IEnumerable<Customar> GetCustomars()
        {
            return _customerRepository.GetCustomars();
        }

        public void Save(Customar obj)
        {
            _customerRepository.Save(obj);
        }
    }
}
