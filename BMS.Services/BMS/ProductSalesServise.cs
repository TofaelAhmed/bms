﻿using BMS.Data.Infrastructure;
using BMS.Data.Repository.BMS;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Services.BMS
{
    public interface IProductSalesServise
    {
        void Save(ProductSales obj);
        void Edit(ProductSales obj);
        IEnumerable<ProductSales> GetProductSales();
    }
    public class ProductSalesServise: IProductSalesServise
    {
        private IProductSalesRepository _productSalesRepository;
        private IUnitOfWork _unitOfWork;
        public ProductSalesServise(IProductSalesRepository productSalesRepository, IUnitOfWork unitOfWork)
        {
            _productSalesRepository = productSalesRepository;
            _unitOfWork = unitOfWork;
        }

        public void Edit(ProductSales obj)
        {
            _productSalesRepository.Update(obj);
        }

        public IEnumerable<ProductSales> GetProductSales()
        {
            return _productSalesRepository.GetAll();
        }

        public void Save(ProductSales obj)
        {
            _productSalesRepository.Add(obj);
        }
    }
}
