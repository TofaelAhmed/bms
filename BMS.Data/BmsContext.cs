﻿using BMS.Data.Mapping.Bms;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data
{
    public class BmsContext:DbContext
    {
        public BmsContext():base("BmsContext")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Customar> Customars { get; set; }
        public DbSet<Employee> Employees { get; set; }
    
        public DbSet<Production> Productions { get; set; }
        public DbSet<ProductDetails> ProductDetails { get; set; }
        public DbSet<ProductSales> ProductSales { get; set; }
        public DbSet<RawMaterial> RawMaterials { get; set; }
        public DbSet<RawMaterialBuy> RawMaterialBuys { get; set; }
        public DbSet<RawMaterialHandover> RawMaterialHandovers { get; set; }
        public DbSet<RequestedProduct> RequestedProducts { get; set; }
        public DbSet<RequestRawMaterial> RequestRawMaterials { get; set; }
        public DbSet<Salary> Salarys { get; set; }
        public DbSet<SalaryPayment> SalaryPayments { get; set; }
        public DbSet<StoredProduct> StoredProducts { get; set; }
        public DbSet<StoredRawMaterial> StoredRawMaterials { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<ProductRawmaterialMapping> ProductRawmaterialMappings { get; set; }
        public DbSet<ProductOrdered> ProductOrdereds { get; set; }
        public virtual void Commit()
        {
            base.SaveChanges();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<ForeignKeyIndexConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
               .Where(type => !String.IsNullOrEmpty(type.Namespace))
               .Where(type => type.BaseType != null && type.BaseType.IsGenericType
                    && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
            base.OnModelCreating(modelBuilder);
        }
    }
}