﻿namespace BMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectSet : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Bms.Customar",
                c => new
                    {
                        CustomarId = c.Int(nullable: false, identity: true),
                        CustomarName = c.String(),
                        CustomerNid = c.String(),
                        CustomarContracNo = c.String(),
                        CustomarAddress = c.String(),
                    })
                .PrimaryKey(t => t.CustomarId);
            
            CreateTable(
                "Bms.Employee",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false, identity: true),
                        EmployeeName = c.String(),
                        EmployeeContactNo = c.String(),
                        EmployeeAddress = c.String(),
                        EmployeeDesignation = c.String(),
                    })
                .PrimaryKey(t => t.EmployeeId);
            
            CreateTable(
                "Bms.ProductDetails",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        UnitId = c.Int(nullable: false),
                        ProductName = c.String(),
                        ProductRemark = c.String(),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("Bms.Unit", t => t.UnitId)
                .Index(t => t.UnitId);
            
            CreateTable(
                "Bms.ProductOrdered",
                c => new
                    {
                        ProductOrderId = c.Int(nullable: false, identity: true),
                        ProductOrderdate = c.DateTime(nullable: false),
                        ProductOrderdeliverydate = c.DateTime(nullable: false),
                        OrderQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductOrderRemark = c.String(),
                        ProductOrderNumber = c.String(),
                        ProductUnitName = c.String(),
                        ProductId = c.Int(nullable: false),
                        IsDelivered = c.Boolean(nullable: false),
                        IsRawRequested = c.Boolean(nullable: false),
                        IsRawDelivered = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProductOrderId)
                .ForeignKey("Bms.ProductDetails", t => t.ProductId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "Bms.ProductWithRawMapping",
                c => new
                    {
                        ProductWithRawMappingId = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        RawMaterialId = c.Int(nullable: false),
                        Quentity = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ProductWithRawMappingId)
                .ForeignKey("Bms.ProductDetails", t => t.ProductId)
                .ForeignKey("Bms.RawMaterial", t => t.RawMaterialId)
                .Index(t => t.ProductId)
                .Index(t => t.RawMaterialId);
            
            CreateTable(
                "Bms.RawMaterial",
                c => new
                    {
                        RawMaterialId = c.Int(nullable: false, identity: true),
                        UnitId = c.Int(nullable: false),
                        RawMaterialName = c.String(),
                        RawMaterialUnit = c.String(),
                        RawMaterialRemark = c.String(),
                    })
                .PrimaryKey(t => t.RawMaterialId)
                .ForeignKey("Bms.Unit", t => t.UnitId)
                .Index(t => t.UnitId);
            
            CreateTable(
                "Bms.RawMaterialBuy",
                c => new
                    {
                        RawMaterialBuyId = c.Int(nullable: false, identity: true),
                        RawMaterialId = c.Int(nullable: false),
                        RawMaterialBuyDate = c.DateTime(nullable: false),
                        RawMaterialBuyRemark = c.String(),
                    })
                .PrimaryKey(t => t.RawMaterialBuyId)
                .ForeignKey("Bms.RawMaterial", t => t.RawMaterialId)
                .Index(t => t.RawMaterialId);
            
            CreateTable(
                "Bms.Unit",
                c => new
                    {
                        UnitId = c.Int(nullable: false, identity: true),
                        UnitName = c.String(),
                    })
                .PrimaryKey(t => t.UnitId);
            
            CreateTable(
                "Bms.Production",
                c => new
                    {
                        ProductionId = c.Int(nullable: false, identity: true),
                        ProductionDate = c.DateTime(nullable: false),
                        ProductionName = c.String(),
                        ProductionUnit = c.String(),
                        ProductionRemark = c.String(),
                    })
                .PrimaryKey(t => t.ProductionId);
            
            CreateTable(
                "Bms.ProductSales",
                c => new
                    {
                        ProductSalesId = c.Int(nullable: false, identity: true),
                        CustomarName = c.String(),
                        ProductSalesDate = c.DateTime(nullable: false),
                        ProductName = c.String(),
                        ProductSalesUnit = c.String(),
                        ProductSalesRate = c.String(),
                        ProductSalesPaid = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ProductSalesId);
            
            CreateTable(
                "Bms.RawMaterialHandover",
                c => new
                    {
                        RawMaterialHandoverId = c.Int(nullable: false, identity: true),
                        RawMaterialHandoverDate = c.DateTime(nullable: false),
                        RawMaterialHandoverName = c.String(),
                        RawMaterialHandoverUnit = c.String(),
                        RawMaterialHandoverRemark = c.String(),
                    })
                .PrimaryKey(t => t.RawMaterialHandoverId);
            
            CreateTable(
                "Bms.RequestedProduct",
                c => new
                    {
                        RequestedProductId = c.Int(nullable: false, identity: true),
                        RequestedProductName = c.String(),
                        RequestedProductDate = c.DateTime(nullable: false),
                        RequestedProductUnit = c.String(),
                        RequestedProductRemark = c.String(),
                    })
                .PrimaryKey(t => t.RequestedProductId);
            
            CreateTable(
                "Bms.RequestRawMaterial",
                c => new
                    {
                        RequestRawMaterialId = c.Int(nullable: false, identity: true),
                        RawMaterialId = c.Int(nullable: false),
                        RequestRawMaterialName = c.Int(nullable: false),
                        RequestRawMaterialDate = c.DateTime(nullable: false),
                        RequestRawMaterialUnit = c.String(),
                        RequestRawMaterialRemark = c.String(),
                    })
                .PrimaryKey(t => t.RequestRawMaterialId)
                .ForeignKey("Bms.RawMaterial", t => t.RawMaterialId)
                .Index(t => t.RawMaterialId);
            
            CreateTable(
                "Bms.SalaryPayment",
                c => new
                    {
                        SalaryPaymentId = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        SalaryPaymentEmpName = c.String(),
                        SalaryPaymentDate = c.DateTime(nullable: false),
                        SalaryPaymentAmount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.SalaryPaymentId)
                .ForeignKey("Bms.Employee", t => t.EmployeeId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Bms.Salary",
                c => new
                    {
                        SalaryId = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        DesignationName = c.String(),
                        SalaryAmount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.SalaryId)
                .ForeignKey("Bms.Employee", t => t.EmployeeId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "Bms.StoredProduct",
                c => new
                    {
                        StoredProductId = c.Int(nullable: false, identity: true),
                        StoredProductName = c.String(),
                        StoredProductRemark = c.String(),
                    })
                .PrimaryKey(t => t.StoredProductId);
            
            CreateTable(
                "Bms.StoredRawMaterial",
                c => new
                    {
                        StoredRawMaterialId = c.Int(nullable: false, identity: true),
                        RawMaterialId = c.Int(nullable: false),
                        StoredRawMaterialName = c.String(),
                        StoredRawMaterialUnit = c.String(),
                    })
                .PrimaryKey(t => t.StoredRawMaterialId)
                .ForeignKey("Bms.RawMaterial", t => t.RawMaterialId)
                .Index(t => t.RawMaterialId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Bms.StoredRawMaterial", "RawMaterialId", "Bms.RawMaterial");
            DropForeignKey("Bms.Salary", "EmployeeId", "Bms.Employee");
            DropForeignKey("Bms.SalaryPayment", "EmployeeId", "Bms.Employee");
            DropForeignKey("Bms.RequestRawMaterial", "RawMaterialId", "Bms.RawMaterial");
            DropForeignKey("Bms.ProductDetails", "UnitId", "Bms.Unit");
            DropForeignKey("Bms.ProductWithRawMapping", "RawMaterialId", "Bms.RawMaterial");
            DropForeignKey("Bms.RawMaterial", "UnitId", "Bms.Unit");
            DropForeignKey("Bms.RawMaterialBuy", "RawMaterialId", "Bms.RawMaterial");
            DropForeignKey("Bms.ProductWithRawMapping", "ProductId", "Bms.ProductDetails");
            DropForeignKey("Bms.ProductOrdered", "ProductId", "Bms.ProductDetails");
            DropIndex("Bms.StoredRawMaterial", new[] { "RawMaterialId" });
            DropIndex("Bms.Salary", new[] { "EmployeeId" });
            DropIndex("Bms.SalaryPayment", new[] { "EmployeeId" });
            DropIndex("Bms.RequestRawMaterial", new[] { "RawMaterialId" });
            DropIndex("Bms.RawMaterialBuy", new[] { "RawMaterialId" });
            DropIndex("Bms.RawMaterial", new[] { "UnitId" });
            DropIndex("Bms.ProductWithRawMapping", new[] { "RawMaterialId" });
            DropIndex("Bms.ProductWithRawMapping", new[] { "ProductId" });
            DropIndex("Bms.ProductOrdered", new[] { "ProductId" });
            DropIndex("Bms.ProductDetails", new[] { "UnitId" });
            DropTable("Bms.StoredRawMaterial");
            DropTable("Bms.StoredProduct");
            DropTable("Bms.Salary");
            DropTable("Bms.SalaryPayment");
            DropTable("Bms.RequestRawMaterial");
            DropTable("Bms.RequestedProduct");
            DropTable("Bms.RawMaterialHandover");
            DropTable("Bms.ProductSales");
            DropTable("Bms.Production");
            DropTable("Bms.Unit");
            DropTable("Bms.RawMaterialBuy");
            DropTable("Bms.RawMaterial");
            DropTable("Bms.ProductWithRawMapping");
            DropTable("Bms.ProductOrdered");
            DropTable("Bms.ProductDetails");
            DropTable("Bms.Employee");
            DropTable("Bms.Customar");
        }
    }
}
