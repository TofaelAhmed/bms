﻿namespace BMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectS : DbMigration
    {
        public override void Up()
        {
            DropColumn("Bms.RawMaterial", "RawMaterialUnit");
        }
        
        public override void Down()
        {
            AddColumn("Bms.RawMaterial", "RawMaterialUnit", c => c.String());
        }
    }
}
