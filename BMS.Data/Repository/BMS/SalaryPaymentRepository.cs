﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface ISalaryPaymentRepository:IRepository<SalaryPayment>
    {
        void Save(SalaryPayment obj);
        void Edit(SalaryPayment obj);
        IEnumerable<SalaryPayment> GetSalaryPayments();
    }
    public class SalaryPaymentRepository : BaseRepository<SalaryPayment>, ISalaryPaymentRepository
    {
        public SalaryPaymentRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(SalaryPayment obj)
        {
            base.Update(obj);
        }

        public IEnumerable<SalaryPayment> GetSalaryPayments()
        {
            return base.GetAll();
        }

        public void Save(SalaryPayment obj)
        {
            base.Add(obj);
        }
    }
}
