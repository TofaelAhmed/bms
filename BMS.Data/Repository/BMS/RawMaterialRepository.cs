﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IRawMaterialRepository: IRepository<RawMaterial>
    {
        void Save(RawMaterial obj);
        void Edit(RawMaterial obj);
        IEnumerable<RawMaterial> GetRawMaterials();
        RawMaterial GetRawMaterialById(int id);
    }
    public class RawMaterialRepository : BaseRepository<RawMaterial>, IRawMaterialRepository
    {
        public RawMaterialRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(RawMaterial obj)
        {
            base.Update(obj);
        }

        public IEnumerable<RawMaterial> GetRawMaterials()
        {
            return BmsContext.RawMaterials.Include("Units").ToList();
        }

        public void Save(RawMaterial obj)
        {
            base.Add(obj);
        }
        public RawMaterial GetRawMaterialById(int id)
        {
            return BmsContext.RawMaterials.Where(x => x.RawMaterialId == id).FirstOrDefault();
        }
    }
}