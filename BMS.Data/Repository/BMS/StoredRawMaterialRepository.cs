﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IStoredRawMaterialRepository:IRepository<StoredRawMaterial>
    {
        void Save(StoredRawMaterial obj);
        void Edit(StoredRawMaterial obj);
        IEnumerable<StoredRawMaterial> GetStoredRawMaterials();
    }
    public class StoredRawMaterialRepository : BaseRepository<StoredRawMaterial>, IStoredRawMaterialRepository
    {
        public StoredRawMaterialRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(StoredRawMaterial obj)
        {
            base.Update(obj);
        }

        public IEnumerable<StoredRawMaterial> GetStoredRawMaterials()
        {
            return base.GetAll();
        }

        public void Save(StoredRawMaterial obj)
        {
            base.Add(obj);
        }
    }
}
