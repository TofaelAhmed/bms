﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IUnitRepository:IRepository<Unit>
    {
        IEnumerable<Unit> GetUnits();
        Unit GetUnitsById(int id);
    }
    public class UnitRepository : BaseRepository<Unit>, IUnitRepository
    {
        public UnitRepository(IDatabaseFactory databaseFactory): base(databaseFactory)
        {

        }
        public IEnumerable<Unit> GetUnits()
        {
            return base.GetAll();
        }
       public Unit GetUnitsById(int id)
        {
            return BmsContext.Units.Where(x => x.UnitId == id).FirstOrDefault();
        }
    }
}
