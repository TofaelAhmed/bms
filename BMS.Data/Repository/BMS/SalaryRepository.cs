﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface ISalaryRepository:IRepository<Salary>
    {
        void Save(Salary obj);
        void Edit(Salary obj);
        IEnumerable<Salary> GetSalaries();
    }
    public class SalaryRepository : BaseRepository<Salary>, ISalaryRepository
    {
        public SalaryRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(Salary obj)
        {
            base.Update(obj);
        }

        public IEnumerable<Salary> GetSalaries()
        {
            return base.GetAll();
        }

        public void Save(Salary obj)
        {
            base.Add(obj);
        }
    }
}
