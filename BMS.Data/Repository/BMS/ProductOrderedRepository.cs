﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IProductOrderedRepository: IRepository<ProductOrdered>
    {
        void Save(ProductOrdered obj);
        List<ProductOrdered> GetByProductId(int id);
        ProductOrdered GetById(int id);
        List<ProductOrdered> PendingOrdered();
        List<ProductOrdered> PendingOrderedRawMaterial();
        void Edit(ProductOrdered item);
    }
    public class ProductOrderedRepository : BaseRepository<ProductOrdered>, IProductOrderedRepository
    {
        public ProductOrderedRepository(IDatabaseFactory databaseFactory):base(databaseFactory)
        {
            
        }
        public void Save(ProductOrdered obj)
        {
            base.Add(obj);
        }
        public List<ProductOrdered> GetByProductId(int id)
        {
            return BmsContext.ProductOrdereds.Include("ProductDetailes").Where(x => x.ProductId == id).ToList();
        }
        public ProductOrdered GetById(int id)
        {
            return BmsContext.ProductOrdereds.Where(x => x.ProductOrderId == id).FirstOrDefault();
        }
        public List<ProductOrdered> PendingOrdered()
        {
            return BmsContext.ProductOrdereds.Include("ProductDetailes").Include("ProductDetailes.Units").Where(x=>x.IsDelivered==false && x.IsRawRequested==false).ToList();
        }
        public List<ProductOrdered> PendingOrderedRawMaterial()
        {
            return BmsContext.ProductOrdereds.Include("ProductDetailes").Include("ProductDetailes.Units")
                .Where(x=>x.IsDelivered==false && x.IsRawDelivered==false).ToList();
        }
        public void Edit(ProductOrdered item)
        {
            base.Update(item);
        }
    }
}