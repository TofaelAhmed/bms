﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IRequestedProductRepository: IRepository<RequestedProduct>
    {
        void Save(RequestedProduct obj);
        void Edit(RequestedProduct obj);
        IEnumerable<RequestedProduct> GetRequestedProducts();
    }
    public class RequestedProductRepository : BaseRepository<RequestedProduct>, IRequestedProductRepository
    {
        public RequestedProductRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(RequestedProduct obj)
        {
            base.Update(obj);
        }

        public IEnumerable<RequestedProduct> GetRequestedProducts()
        {
            return base.GetAll();
        }
         
        public void Save(RequestedProduct obj)
        {
            base.Add(obj);
        }
    }
}
