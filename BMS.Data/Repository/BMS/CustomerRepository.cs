﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface ICustomerRepository:IRepository<Customar>
    {
        void Save(Customar obj);
        void Edit(Customar obj);
        IEnumerable<Customar> GetCustomars();
    }
    public class CustomerRepository : BaseRepository<Customar>, ICustomerRepository
    {
        public CustomerRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }
        public void Edit(Customar obj)
        {
            base.Update(obj);
        }
        public IEnumerable<Customar> GetCustomars()
        {
            return base.GetAll();
        }
        public void Save(Customar obj)
        {
            base.Add(obj);
        }
    }
}
