﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IProductSalesRepository: IRepository<ProductSales>
    {
        void Save(ProductSales obj);
        void Edit(ProductSales obj);
        IEnumerable<ProductSales> GetProductSales();
    }
    public class ProductSalesRepository : BaseRepository<ProductSales>, IProductSalesRepository
    {
        public ProductSalesRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(ProductSales obj)
        {
            base.Update(obj);
        }

        public IEnumerable<ProductSales> GetProductSales()
        {
            return base.GetAll();
        }

        public void Save(ProductSales obj)
        {
            base.Add(obj);
        }
    }
}
