﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IEmployeeRepository:IRepository<Employee>
    {
        void Save(Employee obj);
        void Edit(Employee obj);
        IEnumerable<Employee> GetEmployees();
    }
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }
        public void Edit(Employee obj)
        {
            base.Update(obj);
        }
        public IEnumerable<Employee> GetEmployees()
        {
            return base.GetAll();
        }
        public void Save(Employee obj)
        {
            base.Add(obj);
        }
    }
}
