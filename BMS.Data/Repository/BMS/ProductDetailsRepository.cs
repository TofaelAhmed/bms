﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IProductDetailsRepository: IRepository<ProductDetails>
    {
        void Save(ProductDetails obj);
        void Edit(ProductDetails obj);
        IEnumerable<ProductDetails> GetProductDetails();
        ProductDetails GetProductDetailsById(int id);
    }
    public class ProductDetailsRepository : BaseRepository<ProductDetails>, IProductDetailsRepository
    {
        public ProductDetailsRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(ProductDetails obj)
        {
            base.Update(obj);
        }

        public IEnumerable<ProductDetails> GetProductDetails()
        {
            return base.GetAll();
        }

        public void Save(ProductDetails obj)
        {
            base.Add(obj);
        }
        public ProductDetails GetProductDetailsById(int id)
        {
            return BmsContext.ProductDetails.Where(x => x.ProductId == id).FirstOrDefault();
        }
    }
}