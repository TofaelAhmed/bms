﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IStoredProductRepository:IRepository<StoredProduct>
    {
        void Save(StoredProduct obj);
        void Edit(StoredProduct obj);
        IEnumerable<StoredProduct> GetStoredProducts();
    }
    public class StoredProductRepository : BaseRepository<StoredProduct>, IStoredProductRepository
    {
        public StoredProductRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(StoredProduct obj)
        {
            base.Update(obj);
        }

        public IEnumerable<StoredProduct> GetStoredProducts()
        {
            return base.GetAll();
        }

        public void Save(StoredProduct obj)
        {
            base.Add(obj);
        }
    }
}
