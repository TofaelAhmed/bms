﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IRawMaterialHandoverRepository: IRepository<RawMaterialHandover>
    {
        void save(RawMaterialHandover obj);
        void Edit(RawMaterialHandover obj);
        IEnumerable<RawMaterialHandover> GetRawMaterialHandovers();
    }
    public class RawMaterialHandoverRepository : BaseRepository<RawMaterialHandover>, IRawMaterialHandoverRepository
    {
        public RawMaterialHandoverRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(RawMaterialHandover obj)
        {
            base.Update(obj);
        }

        public IEnumerable<RawMaterialHandover> GetRawMaterialHandovers()
        {
            return base.GetAll();
        }

        public void save(RawMaterialHandover obj)
        {
            base.Add(obj);
        }
    }
}
