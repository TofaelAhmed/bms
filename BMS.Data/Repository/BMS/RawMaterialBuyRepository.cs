﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IRawMaterialBuyRepository: IRepository<RawMaterialBuy>
    {
        void Save(RawMaterialBuy obj);
        void Edit(RawMaterialBuy obj);
        IEnumerable<RawMaterialBuy> GetRawMaterialBuys();
    }
    public class RawMaterialBuyRepository : BaseRepository<RawMaterialBuy>, IRawMaterialBuyRepository
    {
        public RawMaterialBuyRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(RawMaterialBuy obj)
        {
            base.Update(obj);
        }

        public IEnumerable<RawMaterialBuy> GetRawMaterialBuys()
        {
            return base.GetAll();
        }

        public void Save(RawMaterialBuy obj)
        {
            base.Add(obj);
        }
    }
}
