﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IProductRawmaterialMappingRepository : IRepository<Entities.BMS.ProductRawmaterialMapping>
    {
        void Save(Entities.BMS.ProductRawmaterialMapping obj);
        List<ProductRawmaterialMapping> GetProductRawmaterialMappingById(int id);
    }
    public class ProductRawmaterialMappingRepository : BaseRepository<Entities.BMS.ProductRawmaterialMapping>, IProductRawmaterialMappingRepository
    {
        public ProductRawmaterialMappingRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {

        }

        public List<ProductRawmaterialMapping> GetProductRawmaterialMappingById(int id)
        {
            return BmsContext.ProductRawmaterialMappings.Include("ProductDetails").Include("RawMaterials").Where(x => x.ProductId == id).ToList();
        }

        public void Save(Entities.BMS.ProductRawmaterialMapping obj)
        {
            base.Add(obj);
        }
    }
}
