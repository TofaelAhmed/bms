﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IRequestRawMaterialRepository:IRepository<RequestRawMaterial>
    {
        void Save(RequestRawMaterial obj);
        void Edit(RequestRawMaterial obj);
        DataTable GetRequestedRawMaterials();
    }
    public class RequestRawMaterialRepository : BaseRepository<RequestRawMaterial>, IRequestRawMaterialRepository
    {
        public RequestRawMaterialRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(RequestRawMaterial obj)
        {
            base.Update(obj);
        }

        public DataTable GetRequestedRawMaterials()
        {
            DataTable dt = new DataTable();
            dt = GetDataTable("Bms.RequestedRawMatrial", CommandType.StoredProcedure);
            return dt;
        }

        public void Save(RequestRawMaterial obj)
        {
            base.Add(obj);
        }
    }
}
