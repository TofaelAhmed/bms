﻿using BMS.Data.Infrastructure;
using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Repository.BMS
{
    public interface IProductionRepository: IRepository<Production>
    {
        void Save(Production obj);
        void Edit(Production obj);
        IEnumerable<Production> GetProductions();
    }
    public class ProductionRepository : BaseRepository<Production>, IProductionRepository
    {
        public ProductionRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public void Edit(Production obj)
        {
            base.Update(obj);
        }

        public IEnumerable<Production> GetProductions()
        {
            return base.GetAll();
        }

        public void Save(Production obj)
        {
            base.Add(obj);
        }
    }
}
