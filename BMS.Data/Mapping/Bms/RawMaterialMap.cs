﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using BMS.Entities.BMS;
using System.ComponentModel.DataAnnotations.Schema;

namespace BMS.Data.Mapping.Bms
{
    public class RawMaterialMap:EntityTypeConfiguration<RawMaterial>
    {
        public RawMaterialMap()
        {
            HasKey(x => x.RawMaterialId);
            Property(x => x.RawMaterialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.RawMaterial");
        }
    }
}