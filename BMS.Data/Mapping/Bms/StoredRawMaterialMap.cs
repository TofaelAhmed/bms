﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    class StoredRawMaterialMap:EntityTypeConfiguration<StoredRawMaterial>
    {
        public StoredRawMaterialMap()
        {
            HasKey(x => x.StoredRawMaterialId);
            Property(x => x.StoredRawMaterialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.StoredRawMaterial");
        }
    }
}
