﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    class SalaryPaymentMap:EntityTypeConfiguration<SalaryPayment>
    {
        public SalaryPaymentMap()
        {
            HasKey(x => x.SalaryPaymentId);
            Property(x => x.SalaryPaymentId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.SalaryPayment");
        }
    }
}
