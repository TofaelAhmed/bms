﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    public class ProductRawmaterialMappingMap:EntityTypeConfiguration<ProductRawmaterialMapping>
    {
        public ProductRawmaterialMappingMap()
        {
            HasKey(x => x.ProductWithRawMappingId);
            Property(x => x.ProductWithRawMappingId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasRequired(t => t.RawMaterials).WithMany(t => t.ProductRawmaterialMappings).HasForeignKey(d => d.RawMaterialId);
            HasRequired(t => t.ProductDetails).WithMany(t => t.ProductRawmaterialMappings).HasForeignKey(d => d.ProductId);
            ToTable("Bms.ProductWithRawMapping");
        }
    }
}