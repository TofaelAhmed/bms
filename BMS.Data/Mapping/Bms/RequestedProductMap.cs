﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    class RequestedProductMap:EntityTypeConfiguration<RequestedProduct>
    {
        public RequestedProductMap()
        {
            HasKey(x => x.RequestedProductId);
            Property(x => x.RequestedProductId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.RequestedProduct");
        }
    }
}