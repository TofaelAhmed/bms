﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    public class ProductOrderedMap:EntityTypeConfiguration<ProductOrdered>
    {
        public ProductOrderedMap()
        {
            HasKey(x => x.ProductOrderId);
            Property(x => x.ProductOrderId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasRequired(t => t.ProductDetailes).WithMany(t => t.ProductOrdereds).HasForeignKey(d => d.ProductId);
            ToTable("Bms.ProductOrdered");

            
        }
    }
}
