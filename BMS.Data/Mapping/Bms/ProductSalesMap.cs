﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    class ProductSalesMap:EntityTypeConfiguration<ProductSales>
    {
        public ProductSalesMap()
        {
            HasKey(x => x.ProductSalesId);
            Property(x => x.ProductSalesId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.ProductSales");
        }
    }
}
