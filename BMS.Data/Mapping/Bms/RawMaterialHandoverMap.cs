﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    public class RawMaterialHandoverMap:EntityTypeConfiguration<RawMaterialHandover>
    {
        public RawMaterialHandoverMap()
        {
            HasKey(x => x.RawMaterialHandoverId);
            Property(x => x.RawMaterialHandoverId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.RawMaterialHandover");
        }
    }
}
