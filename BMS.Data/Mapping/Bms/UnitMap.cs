﻿using BMS.Entities.BMS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Mapping.Bms
{
    public class UnitMap: EntityTypeConfiguration<Unit>
    {
        public UnitMap()
        {
            HasKey(x => x.UnitId);
            Property(x => x.UnitId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.Unit");
        }
    }
}
