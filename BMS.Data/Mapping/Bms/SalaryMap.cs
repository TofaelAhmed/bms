﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    public class SalaryMap:EntityTypeConfiguration<Salary>
    {
        public SalaryMap()
        {
            HasKey(x => x.SalaryId);
            Property(x => x.SalaryId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.Salary");
        }
    }
}
