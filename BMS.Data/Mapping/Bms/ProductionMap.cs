﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    public class ProductionMap:EntityTypeConfiguration<Production>
    {
        public ProductionMap()
        {
            HasKey(x => x.ProductionId);
            Property(x => x.ProductionId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.Production");
        }
    }
}