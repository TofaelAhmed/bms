﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    class RequestRawMaterialMap:EntityTypeConfiguration<RequestRawMaterial>
    {
        public RequestRawMaterialMap()
        {
            HasKey(x => x.RequestRawMaterialId);
            Property(x => x.RequestRawMaterialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.RequestRawMaterial");
        }
    }
}
