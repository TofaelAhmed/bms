﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    class StoredProductMap:EntityTypeConfiguration<StoredProduct>
    {
        public StoredProductMap()
        {
            HasKey(x => x.StoredProductId);
            Property(x => x.StoredProductId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.StoredProduct");
        }
    }
}
