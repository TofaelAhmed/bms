﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    class RawMaterialBuyMap:EntityTypeConfiguration<RawMaterialBuy>
    {
        public RawMaterialBuyMap()
        {
            HasKey(x => x.RawMaterialBuyId);
            Property(x => x.RawMaterialBuyId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.RawMaterialBuy");
        }
    }
}