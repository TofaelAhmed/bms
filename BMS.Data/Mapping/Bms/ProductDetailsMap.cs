﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using BMS.Entities.BMS;

namespace BMS.Data.Mapping.Bms
{
    public class ProductDetailsMap:EntityTypeConfiguration<ProductDetails>
    {
        public ProductDetailsMap()
        {
            HasKey(x => x.ProductId);
            Property(x => x.ProductId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasRequired(t => t.Units).WithMany(t => t.ProductDetails).HasForeignKey(d => d.UnitId);
            ToTable("Bms.ProductDetails");
        }
    }
}
