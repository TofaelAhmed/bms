﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using BMS.Entities.BMS;
using System.ComponentModel.DataAnnotations.Schema;

namespace BMS.Data.Mapping.Bms
{
    public class CustomarMap:EntityTypeConfiguration<Customar>
    {
        public CustomarMap()
        {
            HasKey(x => x.CustomarId);
            Property(x => x.CustomarId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            ToTable("Bms.Customar");
        }
    }
}