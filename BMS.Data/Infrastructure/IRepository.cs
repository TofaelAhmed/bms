﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Infrastructure
{
    public interface IRepository<T> where T:class
    {
        void Add(T entity);
        void Update(T entity);
        void Update(T entity, List<string> excluded);
        //void Save(T entity);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> where);
        T GetById(long id);
        T GetById(string id);
        T Get(Expression<Func<T, bool>> where);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);
        IEnumerable<T> GetManyWithInclude(Expression<Func<T, bool>> where, string include);
        DataTable GetFromStoredProcedure(string storeProcedure, SqlParameter[] parameters, bool isStoredProcedure = true);
        int ExecuteNonQuery(string storeProcedure, SqlParameter[] parameters, bool isStoredProcedure = true);
        DataSet GetFromStoredProcedureDataset(string storeProcedure, SqlParameter[] parameters, bool isStoredProcedure = true);
        IQueryable<T> Table();
    }
}
