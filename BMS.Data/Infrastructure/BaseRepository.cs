﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Infrastructure
{
    public abstract class BaseRepository<T>: IRepository<T> where T : class
    {
        private BmsContext bmsContext;
        private readonly IDbSet<T> dbset;
        protected BaseRepository(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            dbset = BmsContext.Set<T>();
        }
        protected IDatabaseFactory DatabaseFactory
        {
            get;
            private set;
        }
        public BmsContext BmsContext
        {
            get { return bmsContext ?? (bmsContext = DatabaseFactory.Get()); }
        }
        public virtual void Add(T entity)
        {
            dbset.Add(entity);
        }

        internal void Add(object obj)
        {
            throw new NotImplementedException();
        }

        public virtual void Update(T entity)
        {
            dbset.Attach(entity);
            bmsContext.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Update(T entity, List<string> excluded)
        {
            dbset.Attach(entity);
            bmsContext.Entry(entity).State = EntityState.Modified;
            foreach (var name in excluded)
            {
                bmsContext.Entry(entity).Property(name).IsModified = false;
            }
        }
        public virtual int UpdateRows(T entity)
        {
            dbset.Attach(entity);
            bmsContext.Entry(entity).State = EntityState.Modified;
            return bmsContext.SaveChanges();
        }
        public virtual void Delete(T entity)
        {
            dbset.Remove(entity);
        }
        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbset.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                dbset.Remove(obj);
        }
        public virtual T GetById(long id)
        {
            return dbset.Find(id);
        }
        public virtual T GetById(string id)
        {
            return dbset.Find(id);
        }
        public virtual IEnumerable<T> GetAll()
        {
            return dbset.ToList();
        }
        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).ToList();
        }
        public virtual IEnumerable<T> GetManyWithInclude(Expression<Func<T, bool>> where, string include)
        {
            return dbset.Include(include).Where(where).ToList();
        }
        /// <summary>
        /// Return a paged list of entities
        /// </summary>
        /// <typeparam name="TOrder"></typeparam>
        /// <param name="page">Which page to retrieve</param>
        /// <param name="where">Where clause to apply</param>
        /// <param name="order">Order by to apply</param>
        /// <returns></returns>
        //public virtual IPagedList<T> GetPage<TOrder>(Page page, Expression<Func<T, bool>> where, Expression<Func<T, TOrder>> order)
        //{
        //    var results = dbset.OrderBy(order).Where(where).GetPage(page).ToList();
        //    var total = dbset.Count(where);
        //    return new StaticPagedList<T>(results, page.PageNumber, page.PageSize, total);
        //}

        public T Get(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).FirstOrDefault<T>();
        }

        public void UpdateSecificFeilds<T>(T entity, params Expression<Func<T, object>>[] properties) where T : class
        {
            var db = bmsContext;
            var entry = db.Entry(entity);
            db.Set<T>().Attach(entity);

            foreach (var property in properties)
            {
                //entry.Property(property).IsModified = true;
                entry.ComplexProperty(property).IsModified = true;
            }
            db.Configuration.ValidateOnSaveEnabled = false;
        }
        public int ExecuteNonQuery(string storeProcedure, SqlParameter[] parameters, bool isStoredProcedure = true)
        {
            if (isStoredProcedure)
                return ExecuteNonQueryToInt32(storeProcedure, CommandType.StoredProcedure, parameters);
            else
                return ExecuteNonQueryToInt32(storeProcedure, CommandType.Text, parameters);
        }

        public DataTable GetFromStoredProcedure(string storeProcedure, SqlParameter[] parameters, bool isStoredProcedure = true)
        {
            if (isStoredProcedure)
                return GetDataTable(storeProcedure, CommandType.StoredProcedure, parameters);
            else
                return GetDataTable(storeProcedure, CommandType.Text, parameters);
        }
        public DataSet GetFromStoredProcedureDataset(string storeProcedure, SqlParameter[] parameters, bool isStoredProcedure = true)
        {
            if (isStoredProcedure)
                return GetDataSet(storeProcedure, CommandType.StoredProcedure, parameters);
            else
                return GetDataSet(storeProcedure, CommandType.Text, parameters);
        }

        protected DataTable GetDataTable(string Query, CommandType cmdType, params SqlParameter[] parameters)
        {
            string strCon = bmsContext.Database.Connection.ConnectionString;
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(strCon))
            {
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {
                    cmd.CommandType = cmdType;
                    cmd.Parameters.AddRange(parameters);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }
                }
            }
            return dt;
        }
        protected DataTable GetDataTable(string Query, CommandType cmdType)
        {
            string strCon = bmsContext.Database.Connection.ConnectionString;
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(strCon))
            {
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {
                    cmd.CommandType = cmdType;
                    //cmd.Parameters.AddRange(parameters);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }
                }
            }
            return dt;
        }
        protected DataSet GetDataSet(string Query, CommandType cmdType, params SqlParameter[] parameters)
        {
            string strCon = bmsContext.Database.Connection.ConnectionString;
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(strCon))
            {
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {
                    cmd.CommandType = cmdType;
                    cmd.Parameters.AddRange(parameters);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds);
                    }
                }
            }
            return ds;
        }
        protected Int32 ExecuteScalarToInt32(string Query, CommandType cmdType, params SqlParameter[] parameters)
        {
            string strCon = bmsContext.Database.Connection.ConnectionString;
            using (SqlConnection con = new SqlConnection(strCon))
            {
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {
                    cmd.CommandType = cmdType;
                    cmd.Parameters.AddRange(parameters);
                    CloseOpenConnection(con);
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
        }
        protected Int32 ExecuteNonQueryToInt32(string Query, CommandType cmdType, params SqlParameter[] parameters)
        {
            string strCon = bmsContext.Database.Connection.ConnectionString;
            using (SqlConnection con = new SqlConnection(strCon))
            {
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {
                    cmd.CommandType = cmdType;
                    cmd.Parameters.AddRange(parameters);
                    CloseOpenConnection(con);
                    return Convert.ToInt32(cmd.ExecuteNonQuery());
                }
            }
        }
        protected DataSet ExecuteReader(Type _type, string Query, CommandType cmdType, params SqlParameter[] parameters)
        {
            string strCon = bmsContext.Database.Connection.ConnectionString;
            var ds = new DataSet();
            using (var con = new SqlConnection(strCon))
            {
                using (var cmd = new SqlCommand(Query, con))
                {
                    cmd.CommandType = cmdType;
                    cmd.Parameters.AddRange(parameters);
                    CloseOpenConnection(con);

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds);
                    }
                }
            }
            return ds;
        }
        protected List<T> ExecuteReader<T>(Type _type, string Query, CommandType cmdType, params SqlParameter[] parameters)
        {
            string strCon = bmsContext.Database.Connection.ConnectionString;
            List<T> lst = null;
            using (SqlConnection con = new SqlConnection(strCon))
            {
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {
                    cmd.CommandType = cmdType;
                    cmd.Parameters.AddRange(parameters);
                    CloseOpenConnection(con);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        lst = GetAs<T>(reader);
                    }
                }
            }
            return lst;
        }
        protected List<T> ExecuteReader<T>(Type _type, string Query, CommandType cmdType, ref int rows, params SqlParameter[] parameters)
        {
            string strCon = bmsContext.Database.Connection.ConnectionString;
            List<T> lst = null;
            using (SqlConnection con = new SqlConnection(strCon))
            {
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {
                    cmd.CommandType = cmdType;
                    cmd.Parameters.AddRange(parameters);
                    CloseOpenConnection(con);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        lst = GetAs<T>(reader);
                        if ((reader.NextResult()) && (reader.Read()))
                        {
                            rows = reader.GetInt32(0);
                        }
                    }
                }
            }
            return lst;
        }
        private static List<T> GetAs<T>(SqlDataReader reader)
        {
            List<T> ret = new List<T>();
            T entity;
            // Get all the properties in our Object
            PropertyInfo[] props = typeof(T).GetProperties();
            // For each property get the data from the reader to the object
            List<string> columnList = GetColumnList(reader);

            while (reader.Read())
            {
                entity = Activator.CreateInstance<T>();
                for (int i = 0; i < props.Length; i++)
                {
                    if (columnList.Contains(props[i].Name) && reader[props[i].Name] != DBNull.Value)
                        typeof(T).InvokeMember(props[i].Name, BindingFlags.SetProperty, null, entity, new Object[] { reader[props[i].Name] });
                }
                ret.Add(entity);
            }
            return ret;
        }
        public static List<string> GetColumnList(SqlDataReader reader)
        {
            List<string> columnList = new List<string>();
            DataTable readerSchema = reader.GetSchemaTable();
            for (int i = 0; i < readerSchema.Rows.Count; i++)
                columnList.Add(readerSchema.Rows[i]["ColumnName"].ToString());
            return columnList;
        }
        protected void CloseOpenConnection(SqlConnection con)
        {
            if (con.State != ConnectionState.Closed)
            {
                con.Close();
            }
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
        }
        public virtual IQueryable<T> Table()
        {
            return dbset;
        }
    }
}