﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDatabaseFactory databaseFactory;
        private BmsContext bmsContext;

        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
        }

        protected BmsContext BmsContext
        {
            get { return bmsContext ?? (bmsContext = databaseFactory.Get()); }
        }

        public void Commit()
        {
            try
            {
                BmsContext.SaveChanges();
                BmsContext.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                throw new DbEntityValidationException(GetFullErrorText(ex));
            }

        }
        public string GetFullErrorText(DbEntityValidationException exc)
        {
            var msg = string.Empty;
            foreach (var validationErrors in exc.EntityValidationErrors)
                foreach (var error in validationErrors.ValidationErrors)
                    msg += string.Format("Property: {0} Error: {1}", error.PropertyName, error.ErrorMessage) + Environment.NewLine;
            return msg;
        }
    }
}

