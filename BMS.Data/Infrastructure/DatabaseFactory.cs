﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Data.Infrastructure
{
    public class DatabaseFactory : Disposable,IDatabaseFactory
    {
        private BmsContext bmsContext;
        public BmsContext Get()
        {
            return bmsContext ?? (bmsContext = new BmsContext());
        }
        protected override void DisposeCore()
        {
            if (bmsContext != null)
                bmsContext.Dispose();
        }
    }
}
