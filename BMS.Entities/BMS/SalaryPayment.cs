﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class SalaryPayment
    {
        public int SalaryPaymentId { get; set; }
        public int EmployeeId { get; set; }
        public string SalaryPaymentEmpName { get; set; }
        public DateTime SalaryPaymentDate { get; set; }
        public double SalaryPaymentAmount { get; set; }
        public virtual Employee Employees { get; set; }
    }
}