﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class ProductSales
    {
        public int ProductSalesId { get; set; }
        public string CustomarName { get; set; }
        public DateTime ProductSalesDate { get; set; }
        public string ProductName { get; set; }
        public string ProductSalesUnit { get; set; }
        public string ProductSalesRate { get; set; }
        public double ProductSalesPaid { get; set; }
    }
}
