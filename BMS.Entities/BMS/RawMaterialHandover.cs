﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class RawMaterialHandover
    {
        public int RawMaterialHandoverId { get; set; }
        public DateTime RawMaterialHandoverDate { get; set; }
        public string RawMaterialHandoverName { get; set; }
        public string RawMaterialHandoverUnit { get; set; }
        public string RawMaterialHandoverRemark { get; set; }
    }
}
