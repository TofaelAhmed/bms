﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class StoredProduct
    {
        public int StoredProductId { get; set; }
        public string StoredProductName { get; set; }
        public string StoredProductRemark { get; set; }
    }
}
