﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class ProductRawmaterialMapping
    {
        public int ProductWithRawMappingId { get; set; }
        public int ProductId { get; set; }
        public int RawMaterialId { get; set; }
        public decimal Quentity { get; set; }
        public virtual ProductDetails ProductDetails { get; set; }
        public virtual RawMaterial RawMaterials { get; set; }
    }
}