﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class RawMaterialBuy
    {
        public int RawMaterialBuyId { get; set; }
        public int RawMaterialId { get; set; }
        public DateTime RawMaterialBuyDate { get; set; }
        public string RawMaterialBuyRemark { get; set; }
        public virtual RawMaterial RawMaterials { get; set; }
    }
}