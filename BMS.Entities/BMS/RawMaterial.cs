﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class RawMaterial
    {
        public RawMaterial()
        {
            ProductRawmaterialMappings = new List<ProductRawmaterialMapping>();
            RawMaterialBuys = new List<RawMaterialBuy>();
        }
        public int RawMaterialId { get; set; }
        [Key]
        public int UnitId { get; set; }
        public string RawMaterialName { get; set; }
        //public string RawMaterialUnit { get; set; }
        public string RawMaterialRemark { get; set; }
        public virtual Unit Units { get; set; }
        public virtual ICollection<ProductRawmaterialMapping> ProductRawmaterialMappings { get; set; }
        public virtual ICollection<RawMaterialBuy> RawMaterialBuys { get; set; }
    }
}