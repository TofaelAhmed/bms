﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class ProductOrdered
    {
        public int ProductOrderId { get; set; }
        public DateTime ProductOrderdate { get; set; }
        public DateTime ProductOrderdeliverydate { get; set; }
        public decimal OrderQty { get; set; }
        public string ProductOrderRemark { get; set; }
        public string ProductOrderNumber { get; set; }
        public string ProductUnitName { get; set; }
        public int ProductId { get; set; }
        public bool IsDelivered { get; set; }
        public bool IsRawRequested { get; set; }
        public bool IsRawDelivered { get; set; }
        public virtual ProductDetails ProductDetailes { get; set; }

        //[NotMapped]
        //public string Productname { get; set; }
    }
}
