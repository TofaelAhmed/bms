﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class RequestedProduct
    {
        public int RequestedProductId { get; set; }
        public string RequestedProductName { get; set; }
        public DateTime RequestedProductDate { get; set; }
        public string RequestedProductUnit { get; set; }
        public string RequestedProductRemark { get; set; }
    }
}