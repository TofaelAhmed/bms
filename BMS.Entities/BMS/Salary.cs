﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class Salary
    {
        public int SalaryId { get; set; }
        public int EmployeeId { get; set; }
        public string DesignationName { get; set; }
        public double SalaryAmount { get; set; }
        public virtual Employee Employees { get; set; }
    }
}