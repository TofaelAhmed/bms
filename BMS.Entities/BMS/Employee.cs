﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BMS.Entities.BMS
{
    public class Employee
    {
        public Employee()
        {
        }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeContactNo { get; set; }
        public string EmployeeAddress { get; set; }
        public string EmployeeDesignation { get; set; }
    }
}
