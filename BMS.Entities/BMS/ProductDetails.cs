﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class ProductDetails
    {
        public ProductDetails()
        {
            ProductOrdereds = new List<ProductOrdered>();
            ProductRawmaterialMappings = new List<ProductRawmaterialMapping>();
        }
        [Key]
        public int ProductId { get; set; }
        public int UnitId { get; set; }
        public string ProductName { get; set; }
        public string ProductRemark { get; set; }
        public virtual Unit Units { get; set; }
        public virtual ICollection<ProductOrdered> ProductOrdereds { get; set; }
        public virtual ICollection<ProductRawmaterialMapping> ProductRawmaterialMappings { get; set; }
    }
}