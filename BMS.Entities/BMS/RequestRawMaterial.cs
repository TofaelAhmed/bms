﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class RequestRawMaterial
    {
        public int RequestRawMaterialId { get; set; }
        public int RawMaterialId { get; set; }
        public int RequestRawMaterialName { get; set; }
        public DateTime RequestRawMaterialDate { get; set; }
        public string RequestRawMaterialUnit { get; set; }
        public string RequestRawMaterialRemark { get; set; }
        public virtual RawMaterial RawMaterials { get; set; }
    }
}