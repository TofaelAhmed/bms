﻿
using BMS.Entities.Cmn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class Customar
    {
        public Customar()
        {

        }
        public int CustomarId { get; set; }
        public string CustomarName { get; set; }
        public string CustomerNid { get; set; }
        public string CustomarContracNo { get; set; }
        public string CustomarAddress { get; set; }
    }
}