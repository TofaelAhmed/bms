﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class Unit
    {
        public Unit()
        {
            ProductDetails = new List<ProductDetails>();
            RawMaterials = new List<RawMaterial>();
        }
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public virtual ICollection<ProductDetails> ProductDetails { get; set; }
        public virtual ICollection<RawMaterial> RawMaterials { get; set; }
    }
}
