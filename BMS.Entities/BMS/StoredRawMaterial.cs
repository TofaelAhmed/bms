﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class StoredRawMaterial
    {
        public int StoredRawMaterialId { get; set; }
        public int RawMaterialId { get; set; }
        public string StoredRawMaterialName { get; set; }
        public string StoredRawMaterialUnit { get; set; }
        public virtual RawMaterial RawMaterials { get; set; }
    }
}