﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.BMS
{
    public class Production
    {
        public int ProductionId { get; set; }
        public DateTime ProductionDate { get; set; }
        public string ProductionName { get; set; }
        public string ProductionUnit { get; set; }
        public string ProductionRemark { get; set; }
    }
}