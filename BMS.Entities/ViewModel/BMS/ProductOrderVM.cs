﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.ViewModel.BMS
{
    public class ProductOrderVM
    {
        public string OrderNumber { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public decimal ProductQuentity { get; set; }
        public string ProductUnit { get; set; }
        public string ProductRemark { get; set; }
    }
}
