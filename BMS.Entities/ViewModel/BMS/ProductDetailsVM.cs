﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Entities.ViewModel.BMS
{
    public class ProductDetailsVM
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int RawatarialId { get; set; }
        public string RawatarialName { get; set; }
        public decimal Quantity { get; set; }
        public string UnitName { get; set; }
    }
}
