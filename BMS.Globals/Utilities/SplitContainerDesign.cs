﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMS.Globals.Utilities
{
    public class SplitContainerDesign
    {
        private static SplitContainerDesign _obj;
        public static SplitContainerDesign Instance(SplitContainer splitContainer)
        {
            if (_obj != null)
            {
                _obj = null;
            }
            return (_obj = new SplitContainerDesign(splitContainer));
        }

        public SplitContainerDesign(SplitContainer splitContainer) :base()
        {
            splitContainer.BorderStyle = BorderStyle.FixedSingle;
            splitContainer.Panel1.BackColor = Color.FromArgb(227,227,227);
        }
    }
}
