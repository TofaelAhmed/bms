namespace LoadingClass
{
    partial class LoadingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loderInabePictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.loderInabePictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // loderInabePictureBox
            // 
            this.loderInabePictureBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loderInabePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.loderInabePictureBox.Image = global::BMS.Globals.Properties.Resources.loading;
            this.loderInabePictureBox.Location = new System.Drawing.Point(14, 10);
            this.loderInabePictureBox.Name = "loderInabePictureBox";
            this.loderInabePictureBox.Size = new System.Drawing.Size(116, 112);
            this.loderInabePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.loderInabePictureBox.TabIndex = 3;
            this.loderInabePictureBox.TabStop = false;
            // 
            // LoadingForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(145, 133);
            this.Controls.Add(this.loderInabePictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoadingForm";
            this.Opacity = 0.8D;
            this.Padding = new System.Windows.Forms.Padding(4);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoadingForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.LoadingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.loderInabePictureBox)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.PictureBox loderInabePictureBox;
    }
}