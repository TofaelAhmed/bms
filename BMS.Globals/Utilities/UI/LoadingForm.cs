using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LoadingClass
{
    public partial class LoadingForm : Form
    {
        public LoadingForm()
        {
            InitializeComponent();
        }
        
        private void LoadingForm_Load(object sender, EventArgs e)
        {
            int a = loderInabePictureBox.Width - loderInabePictureBox.Image.Width;
            int b = loderInabePictureBox.Height - loderInabePictureBox.Image.Height;
            Padding p = new System.Windows.Forms.Padding();
            p.Left = a / 2;
            p.Top = b / 2;
            loderInabePictureBox.Padding = p;
        }
    }
}