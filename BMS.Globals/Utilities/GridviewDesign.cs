﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;



namespace BMS.Globals.Utilities
{
    public class GridviewDesign

    {
        private static GridviewDesign _obj;
        public static GridviewDesign Instance(DataGridView gridViewName)
        {
            if (_obj != null)
            {
                _obj = null;
            }
            return (_obj = new GridviewDesign(gridViewName));
        }
        public GridviewDesign(DataGridView gridViewName):base()
        {
            gridViewName.BorderStyle = BorderStyle.FixedSingle;
            gridViewName.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            gridViewName.CellBorderStyle = DataGridViewCellBorderStyle.Raised;
            gridViewName.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            gridViewName.DefaultCellStyle.SelectionForeColor = Color.AliceBlue;
            gridViewName.BackgroundColor = Color.FromArgb(227,227,227);
            gridViewName.EnableHeadersVisualStyles = false;
            gridViewName.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            gridViewName.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            gridViewName.ColumnHeadersDefaultCellStyle.ForeColor = Color.Coral;
            gridViewName.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            gridViewName.Enabled = true;
            //gridViewName.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //gridViewName.AllowUserToAddRows = false;
            gridViewName.Font = new Font("Times New Roman", 12);
        }
        public GridviewDesign()
        {

        }
    }
}