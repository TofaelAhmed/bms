﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Globals.Utilities
{
    public class VirtualModel3
    {
        public DateTime Date { get; set; }
        public string Customar { get; set; }
        public string ContractNo { get; set; }
        public string Productname { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public string UnitName { get; set; }
        public decimal Commition { get; set; }
        public decimal Total { get; set; }
    }
}
