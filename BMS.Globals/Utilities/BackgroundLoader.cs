﻿using LoadingClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Globals.Utilities
{
    public class BackgroundLoader
    {
        public delegate void RunFunction();

        public BackgroundWorker Bw;
        public RunFunction thisFunction;
        LoadingForm newLoading;

        public BackgroundLoader(RunFunction newFunction)
        {
            thisFunction = newFunction;
            Bw = new BackgroundWorker();
            Bw.DoWork += new DoWorkEventHandler(Bw_DoWork);
            Bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Bw_RunWorkerCompleted);
        }

        public void Start()
        {
            Bw.RunWorkerAsync();
            newLoading = new LoadingForm();
            newLoading.ShowDialog();
        }
        void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            newLoading.Dispose();
            //MessageBox.Show("Complete");
        }

        void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            if (thisFunction != null)
            {
                thisFunction();
            }
        }
    }
}
