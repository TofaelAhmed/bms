﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMS.Globals.Utilities
{
    public class GridviewDisplay
    {
        private static GridviewDisplay _obj;
        public static GridviewDisplay Instance(DataGridView gridViewName)
        {
            if (_obj != null)
            {
                _obj = null;
            }
            return (_obj = new GridviewDisplay(gridViewName));
        }
        public GridviewDisplay(DataGridView gridViewName) :base()
        {
            gridViewName.AutoGenerateColumns = false;
        }
    }
}
