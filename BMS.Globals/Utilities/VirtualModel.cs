﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMS.Globals.Utilities
{
    public class VirtualModel
    {
        public DateTime Date { get; set; }
        public string RawMatarialName { get; set; }
        public string Unit { get; set; }
        public decimal Qty { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
        public int Comition { get; set; }
        public decimal subtotal { get; set; }
    }
}